# Project knowledge base

## General information

The goal of the project is to develop a server, as well as a frontend part of the application for the "Barney's Gourmet Hamburgers" fast food restaurant chain.

Time limits - deadline date is 02/28/2023.

<strong>Clients:</strong>
* Syberry Academy
* Syberry Production

## Documentation
1. [Functional requirements](documents/functional_requirements.md)
2. [Technical design](documents/technical_design.md)
3. [Technology stack](documents/technology_stack.md)
4. [DB schema](documents/assets/images/db_erd.png)
5. [API documentation](https://documenter.getpostman.com/view/18726292/2s93CRKWo2)
