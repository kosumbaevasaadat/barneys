export const DB_TYPE = 'DB_TYPE';
export const DB_URL = 'DB_URL';
export const DB_PORT = 'DB_PORT';
export const DB_USER = 'DB_USER';
export const DB_PASS = 'DB_PASS';
export const DB_NAME = 'DB_NAME';
export const DB_SYNCHRONIZE = 'DB_SYNCHRONIZE';
export const DEV = 'DEV';
