import { config } from 'dotenv';
import * as cookieParser from 'cookie-parser';
import * as passport from 'passport';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';

import { AppModule } from './app.module';

async function bootstrap() {
  config();

  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const PORT = configService.get<number>('PORT') || 3000;
  app.setGlobalPrefix('api');
  app.use(passport.initialize());
  app.use(cookieParser());
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors({ origin: '*', credentials: true, allowedHeaders: '*' });
  await app.listen(PORT, () =>
    console.log(`Application started on port ${PORT}`),
  );
}

void bootstrap();
