import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  forwardRef,
  Inject,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';

import { BaseService } from '../../shared/lib/base.service';
import { LocationAddress } from '../../entities/location-address.entity';
import { CreateLocationAddressDto } from './dto/create-location-address.dto';
import { alreadyExist, notExist } from '../../shared/lib/functions';
import { CityService } from '../city/city.service';
import { StateService } from '../state/state.service';
import { EntityStatus } from '../../shared/enums/entity-status';
import { UpdateLocationAddressDto } from './dto/update-location-address.dto';
import { RegionService } from '../region/region.service';

@Injectable()
export class LocationAddressService extends BaseService<LocationAddress> {
  constructor(
    @InjectRepository(LocationAddress)
    private locationAddressRepository: Repository<LocationAddress>,
    @Inject(forwardRef(() => CityService))
    private cityService: CityService,
    @Inject(forwardRef(() => StateService))
    private stateService: StateService,
    private readonly regionService: RegionService,
  ) {
    super(locationAddressRepository, 'locationAddress');
  }

  async getAll() {
    return this.locationAddressRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  async getByCityId(id: number) {
    return this.locationAddressRepository
      .createQueryBuilder('locationAddress')
      .leftJoinAndSelect(`locationAddress.city`, 'city')
      .where('city.id LIKE :id', { id })
      .getMany();
  }

  async getByStateId(id: number) {
    return this.locationAddressRepository
      .createQueryBuilder('locationAddress')
      .leftJoinAndSelect(`locationAddress.state`, 'state')
      .where('state.id LIKE :id', { id })
      .getMany();
  }

  async updateManyByCity(id: number) {
    const addressWithRelations = await this.getByCityId(id);
    for (const address of addressWithRelations) {
      address.city = null;
      await this.locationAddressRepository.save(address);
    }
  }

  async updateManyByState(id: number) {
    const addressWithRelations = await this.getByStateId(id);
    for (const address of addressWithRelations) {
      address.state = null;
      await this.locationAddressRepository.save(address);
    }
  }

  async create(createLocationAddressDto: CreateLocationAddressDto) {
    const existAddress = await this.getByMultipleProperties([
      {
        key: 'street',
        value: createLocationAddressDto.street,
      },
      {
        key: 'building',
        value: createLocationAddressDto.building,
      },
    ]);
    if (existAddress)
      throw new UnprocessableEntityException([
        alreadyExist('location address'),
      ]);

    const city = await this.cityService.getBy(
      { key: 'id', value: createLocationAddressDto.cityId },
      false,
    );
    if (!city) throw new UnprocessableEntityException([notExist('city')]);
    delete createLocationAddressDto.cityId;

    const state = await this.stateService.getBy(
      { key: 'id', value: createLocationAddressDto.stateId },
      false,
    );
    if (!state) throw new UnprocessableEntityException([notExist('state')]);
    delete createLocationAddressDto.stateId;

    const region = await this.regionService.checkExist(
      createLocationAddressDto.regionId,
    );
    delete createLocationAddressDto.regionId;

    const address = this.locationAddressRepository.create({
      ...createLocationAddressDto,
      city,
      state,
      region,
    });
    return this.locationAddressRepository.save(address);
  }

  async update(id: number, updateLocationAddressDto: UpdateLocationAddressDto) {
    const existAddress = await this.getBy({ key: 'id', value: id });
    if (!existAddress)
      throw new UnprocessableEntityException([notExist('address')]);

    const updateData = updateLocationAddressDto;

    if (updateLocationAddressDto.cityId) {
      const city = await this.cityService.getBy(
        { key: 'id', value: updateLocationAddressDto.cityId },
        false,
      );
      if (!city) throw new UnprocessableEntityException([notExist('city')]);
      delete updateData.cityId;
      updateData['city'] = city;
    }
    if (updateLocationAddressDto.stateId) {
      const state = await this.stateService.getBy(
        { key: 'id', value: updateLocationAddressDto.stateId },
        false,
      );
      if (!state) throw new UnprocessableEntityException([notExist('state')]);
      delete updateData.stateId;
      updateData['state'] = state;
    }

    if (updateLocationAddressDto.regionId) {
      const region = await this.regionService.checkExist(
        updateLocationAddressDto.regionId,
      );
      delete updateData.regionId;
      updateData['region'] = region;
    }

    const address = this.locationAddressRepository.merge(
      existAddress,
      updateData,
    );
    return this.locationAddressRepository.save(address);
  }

  async remove(id: number) {
    const existAddress = await this.getBy({ key: 'id', value: id });
    if (!existAddress)
      throw new UnprocessableEntityException([notExist('address')]);

    const address = this.locationAddressRepository.merge(existAddress, {
      status: EntityStatus.DELETED,
    });
    return this.locationAddressRepository.save(address);
  }
}
