import { IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateLocationAddressDto {
  @IsString()
  @IsNotEmpty()
  street: string;

  @IsInt()
  @IsNotEmpty()
  building: number;

  @IsInt()
  @IsOptional()
  unit?: number = null;

  @IsInt()
  @IsNotEmpty()
  zipCode: number;

  @IsInt()
  @IsNotEmpty()
  cityId: number;

  @IsInt()
  @IsNotEmpty()
  stateId: number;

  @IsInt()
  @IsNotEmpty()
  regionId: number;
}
