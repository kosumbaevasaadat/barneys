import { PartialType } from '@nestjs/mapped-types';
import { CreateLocationAddressDto } from './create-location-address.dto';

export class UpdateLocationAddressDto extends PartialType(
  CreateLocationAddressDto,
) {}
