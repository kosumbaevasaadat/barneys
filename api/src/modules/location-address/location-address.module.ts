import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LocationAddress } from '../../entities/location-address.entity';
import { LocationAddressService } from './location-address.service';
import { LocationAddressController } from './location-address.controller';
import { CityModule } from '../city/city.module';
import { StateModule } from '../state/state.module';
import { RegionModule } from '../region/region.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([LocationAddress]),
    forwardRef(() => CityModule),
    forwardRef(() => StateModule),
    RegionModule,
  ],
  providers: [LocationAddressService],
  controllers: [LocationAddressController],
  exports: [LocationAddressService],
})
export class LocationAddressModule {}
