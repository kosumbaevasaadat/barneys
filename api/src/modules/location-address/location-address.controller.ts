import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';

import { LocationAddressService } from './location-address.service';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { RoleGuard } from '../auth/guards/role.guard';
import { Role } from '../../shared/decorators/role.decorator';
import { CreateLocationAddressDto } from './dto/create-location-address.dto';
import { Roles } from '../../shared/enums/user-roles';
import { UpdateLocationAddressDto } from './dto/update-location-address.dto';

@Controller('location-address')
export class LocationAddressController {
  constructor(private locationAddressService: LocationAddressService) {}

  @Get()
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  getAll() {
    return this.locationAddressService.getAll();
  }

  @Post()
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  create(@Body() createLocationAddressDto: CreateLocationAddressDto) {
    return this.locationAddressService.create(createLocationAddressDto);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  update(
    @Body() updateLocationAddressDto: UpdateLocationAddressDto,
    @Param('id', ParseIntPipe) id: number,
  ) {
    return this.locationAddressService.update(id, updateLocationAddressDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.locationAddressService.remove(id);
  }
}
