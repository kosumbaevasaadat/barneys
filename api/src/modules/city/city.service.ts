import { Repository } from 'typeorm';
import {
  forwardRef,
  Inject,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { City } from '../../entities/city.entity';
import { CreateCityDto } from './dto/create-city.dto';
import { BaseService } from '../../shared/lib/base.service';
import {
  alreadyExist,
  deletedSuccessfully,
  notExist,
  sameExist,
} from '../../shared/lib/functions';
import { EntityStatus } from '../../shared/enums/entity-status';
import { UpdateCityDto } from './dto/update-city.dto';
import { LocationAddressService } from '../location-address/location-address.service';

@Injectable()
export class CityService extends BaseService<City> {
  constructor(
    @InjectRepository(City)
    private cityRepository: Repository<City>,
    @Inject(forwardRef(() => LocationAddressService))
    private locationAddressService: LocationAddressService,
  ) {
    super(cityRepository, 'city');
  }

  async get() {
    return this.cityRepository.find({ where: { status: EntityStatus.ACTIVE } });
  }

  async create(createCityDto: CreateCityDto) {
    const existCity = await this.getBy({
      key: 'title',
      value: createCityDto.title,
    });
    if (existCity)
      throw new UnprocessableEntityException([alreadyExist('city')]);

    const city = this.cityRepository.create(createCityDto);
    return this.cityRepository.save(city);
  }

  async update(id: number, updateCityDto: UpdateCityDto) {
    const existCity = await this.getBy({ key: 'id', value: id });
    if (!existCity) throw new UnprocessableEntityException([notExist('city')]);

    const sameCity = await this.getBy({
      key: 'title',
      value: updateCityDto.title,
    });
    if (sameCity) throw new UnprocessableEntityException([sameExist('city')]);

    const city = this.cityRepository.merge(existCity, updateCityDto);
    return this.cityRepository.save(city);
  }

  async remove(id: number) {
    const existCity = await this.getBy({ key: 'id', value: id });
    if (!existCity) throw new UnprocessableEntityException([notExist('city')]);

    const city = this.cityRepository.merge(existCity, {
      status: EntityStatus.DELETED,
    });
    await this.locationAddressService.updateManyByCity(id);
    await this.repository.save(city);

    return { message: deletedSuccessfully('city', id) };
  }
}
