import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { City } from '../../entities/city.entity';
import { CityService } from './city.service';
import { CityController } from './city.controller';
import { LocationAddressModule } from '../location-address/location-address.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([City]),
    forwardRef(() => LocationAddressModule),
  ],
  providers: [CityService],
  controllers: [CityController],
  exports: [CityService],
})
export class CityModule {}
