import { Roles } from '../../shared/enums/user-roles';

export const ADMIN_PASSWORD = 'ADMIN_PASSWORD';
export const ADMIN_EMAIL = 'ADMIN_EMAIL';
export const ADMIN_DATA = {
  firstName: 'admin',
  lastName: 'admin',
  role: Roles.ADMIN,
  phoneNumber: '+1 (123) 123-1234',
};
