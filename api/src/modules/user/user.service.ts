import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { BadRequestException, forwardRef, Inject, Injectable, } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';

import { User } from '../../entities/user.entity';
import { BaseService } from '../../shared/lib/base.service';
import { SignUpDto } from '../auth/dto/sign-up.dto';
import { ERRORS, USER } from '../../shared/constants';
import { UpdateUserDto } from './dto/update-user.dto';
import { CardService } from '../card/card.service';
import { AddressService } from '../address/address.service';
import { UpdateEmailDto } from './dto/update-email.dto';
import { AuthService } from '../auth/auth.service';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { EntityStatus } from '../../shared/enums/entity-status';
import { Address } from '../../entities/address.entity';

@Injectable()
export class UserService extends BaseService<User> {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private configService: ConfigService,
    private cardService: CardService,
    private addressService: AddressService,
    @Inject(forwardRef(() => AuthService))
    private authService: AuthService,
  ) {
    super(userRepository, 'user');
  }

  async getOne(user: User) {
    const userData = await this.getByWithRelations(
      { key: 'id', value: user.id },
      ['card', 'address'],
    );
    return this.removePasswordOnRes(userData);
  }

  async create(user: SignUpDto) {
    const existedUser = await this.getBy({
      key: USER.email,
      value: user.email,
    });

    if (existedUser) {
      throw new BadRequestException([ERRORS.USER.alreadyExist]);
    }

    const userData = this.userRepository.create(user);
    await this.userRepository.save(userData);
    return this.removePasswordOnRes(userData);
  }

  async update(user: User, updateUserDto: UpdateUserDto) {
    const userWithRelations = await this.getByWithRelations(
      { key: 'id', value: user.id },
      ['card', 'address'],
    );

    if (updateUserDto.card) {
      const card = await this.cardService.create(user, updateUserDto.card);
      updateUserDto.card = card;
    } else if (!updateUserDto.card) {
      await this.cardService.remove(user.id);
    }

    if (updateUserDto.address) {
      let address: Address;
      if (userWithRelations.address) {
        address = await this.addressService.update(
          userWithRelations.address.id,
          { ...updateUserDto.address, status: EntityStatus.ACTIVE },
        );
      } else {
        address = await this.addressService.create(updateUserDto.address);
      }
      updateUserDto.address = address;
    } else if (!updateUserDto.address) {
      await this.addressService.remove(userWithRelations.address.id);
    }

    const userData = this.userRepository.merge(user, updateUserDto);
    await this.userRepository.save(userData);
    return this.removePasswordOnRes(userData);
  }

  async updateEmail(user: User, updateEmailDto: UpdateEmailDto) {
    await this.authService.verifyPassword(
      updateEmailDto.password,
      user.password,
    );

    const existEmail = await this.getBy({
      key: 'email',
      value: updateEmailDto.email,
    });
    if (existEmail) throw new BadRequestException([ERRORS.USER.emailExist]);

    const userData = this.userRepository.merge(user, {
      email: updateEmailDto.email,
    });
    await this.userRepository.save(userData);
    return this.removePasswordOnRes(userData);
  }

  async updatePassword(user: User, updatePasswordDto: UpdatePasswordDto) {
    await this.authService.verifyPassword(
      updatePasswordDto.oldPassword,
      user.password,
    );

    const encodedPassword = await this.encodePassword(
      updatePasswordDto.newPassword,
    );

    const userData = this.userRepository.merge(user, encodedPassword);
    await this.userRepository.save(userData);
    return this.removePasswordOnRes(userData);
  }

  async encodePassword(password: string) {
    const encodedPassword = {
      password: (await bcrypt.hash(password, 10)) as string,
    };
    return encodedPassword;
  }

  removePasswordOnRes(user: User) {
    delete user.password;
    return user;
  }
}
