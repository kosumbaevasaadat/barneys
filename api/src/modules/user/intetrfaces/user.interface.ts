import { PaymentMethod } from '../../../shared/enums/payment-method';
import { Roles } from '../../../shared/enums/user-roles';
import { Address } from '../../../entities/address.entity';
import { BaseInterface } from '../../../shared/interfaces/base.interface';

export interface UserInterface extends BaseInterface {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phoneNumber: string;
  paymentMethod: PaymentMethod;
  role: Roles;
  address?: Address;
}
