import { IsNotEmpty, IsString, MinLength } from 'class-validator';

import { IsPhoneNumber } from '../../../shared/decorators/is-phone-number';
import { PaymentMethod } from '../../../shared/enums/payment-method';
import { Address } from '../../../entities/address.entity';
import { Card } from '../../../entities/card.entity';

export class UpdateUserDto {
  @MinLength(1)
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @MinLength(1)
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @IsPhoneNumber()
  @IsNotEmpty()
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  paymentMethod: PaymentMethod;

  address: null | Address;

  card: null | Card;
}
