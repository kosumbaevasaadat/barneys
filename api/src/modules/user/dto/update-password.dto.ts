import { IsNotEmpty, IsString } from 'class-validator';
import { IsPassword } from '../../../shared/decorators/is-password';

export class UpdatePasswordDto {
  @IsString()
  @IsNotEmpty()
  @IsPassword()
  oldPassword: string;

  @IsString()
  @IsNotEmpty()
  @IsPassword()
  newPassword: string;
}
