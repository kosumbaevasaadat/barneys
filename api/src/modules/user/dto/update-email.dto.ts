import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { IsPassword } from '../../../shared/decorators/is-password';

export class UpdateEmailDto {
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @IsPassword()
  password: string;
}
