import { Body, Controller, Get, Patch, UseGuards } from '@nestjs/common';

import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { ReqUser } from '../../shared/decorators/req-user';
import { User } from '../../entities/user.entity';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateEmailDto } from './dto/update-email.dto';
import { UpdatePasswordDto } from './dto/update-password.dto';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('me')
  @UseGuards(JwtAuthGuard)
  getProfile(@ReqUser() user: User) {
    return this.userService.getOne(user);
  }

  @Patch()
  @UseGuards(JwtAuthGuard)
  update(@ReqUser() user: User, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(user, updateUserDto);
  }

  @Patch('email')
  @UseGuards(JwtAuthGuard)
  updateEmail(@ReqUser() user: User, @Body() updateEmailDto: UpdateEmailDto) {
    return this.userService.updateEmail(user, updateEmailDto);
  }

  @Patch('password')
  @UseGuards(JwtAuthGuard)
  updatePassword(
    @ReqUser() user: User,
    @Body() updatePasswordDto: UpdatePasswordDto,
  ) {
    return this.userService.updatePassword(user, updatePasswordDto);
  }
}
