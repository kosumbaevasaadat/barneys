import { IsInt, IsNotEmpty } from 'class-validator';

export class CreatePromotionDto {
  @IsInt()
  @IsNotEmpty()
  fileId: number;
}
