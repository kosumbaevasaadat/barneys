import { Repository } from 'typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreatePromotionDto } from './dto/create-promotion.dto';
import { Promotion } from '../../entities/promotion.entity';
import { alreadyExist, deletedSuccessfully } from '../../shared/lib/functions';
import { FileService } from '../file/file.service';
import { BaseService } from '../../shared/lib/base.service';
import { EntityStatus } from '../../shared/enums/entity-status';

@Injectable()
export class PromotionService extends BaseService<Promotion> {
  constructor(
    @InjectRepository(Promotion)
    private readonly promotionRepository: Repository<Promotion>,
    private readonly fileService: FileService,
  ) {
    super(promotionRepository, 'promotion');
  }

  async create(createPromotionDto: CreatePromotionDto) {
    const existPromotion = await this.promotionRepository
      .createQueryBuilder('promotion')
      .leftJoinAndSelect('promotion.file', 'file')
      .where('file.id = :id', { id: createPromotionDto.fileId })
      .getOne();
    if (existPromotion)
      throw new UnprocessableEntityException(alreadyExist('promotion'));

    const file = await this.fileService.checkExist(createPromotionDto.fileId);

    const promotion = this.promotionRepository.create({ image: file });
    return this.promotionRepository.save(promotion);
  }

  findAll() {
    return this.promotionRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  findOne(id: number) {
    return this.checkExist(id);
  }

  async remove(id: number) {
    const existPromotion = await this.checkExist(id);
    await this.promotionRepository.merge(existPromotion, {
      status: EntityStatus.DELETED,
    });
    return deletedSuccessfully('promotion', id);
  }
}
