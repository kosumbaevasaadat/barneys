import { Body, Controller, Get, Patch, UseGuards } from '@nestjs/common';
import { CartService } from './cart.service';
import { UpdateCartDto } from './dto/update-cart.dto';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { ReqUser } from '../../shared/decorators/req-user';
import { User } from '../../entities/user.entity';

@Controller('cart')
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  findOne(@ReqUser() user: User) {
    return this.cartService.getByUserId(user.id);
  }

  @Patch()
  @UseGuards(JwtAuthGuard)
  update(@ReqUser() user: User, @Body() updateCartDto: UpdateCartDto) {
    return this.cartService.update(user, updateCartDto);
  }
}
