import { IsEnum, IsNotEmpty } from 'class-validator';
import { ProceedStatus } from '../../../shared/enums/entity-status';

export class UpdateCartDto {
  @IsNotEmpty()
  @IsEnum(ProceedStatus)
  proceedStatus: ProceedStatus;
}
