import { Repository } from 'typeorm';
import {
  forwardRef,
  Inject,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateCartDto } from './dto/create-cart.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import { Cart } from '../../entities/cart.entity';
import { UserService } from '../user/user.service';
import { EntityStatus, ProceedStatus } from '../../shared/enums/entity-status';
import { notExist } from '../../shared/lib/functions';
import { User } from '../../entities/user.entity';

@Injectable()
export class CartService {
  constructor(
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
  ) {}

  async create(createCartDto: CreateCartDto) {
    const existUser = await this.userService.checkExist(createCartDto.userId);

    const cart = this.cartRepository.create({
      user: existUser,
      proceedStatus: ProceedStatus.ACTIVE,
    });
    return this.cartRepository.save(cart);
  }

  async getByUserId(userId: number) {
    const cart = await this.cartRepository
      .createQueryBuilder('cart')
      .where('cart.status = :status', { status: EntityStatus.ACTIVE })
      .leftJoinAndSelect('cart.products', 'product')
      .leftJoinAndSelect('product.product', 'dish')
      .leftJoinAndSelect('dish.file', 'file')
      .leftJoin('cart.user', 'user')
      .andWhere('user.id = :id', { id: userId })
      .getOne();

    if (!cart) throw new UnprocessableEntityException(notExist('cart'));

    return cart;
  }

  async update(user: User, updateCartDto: UpdateCartDto) {
    const existCart = await this.getByUserId(user.id);

    const cart = this.cartRepository.merge(existCart, {
      proceedStatus: updateCartDto.proceedStatus,
    });
    return this.cartRepository.save(cart);
  }
}
