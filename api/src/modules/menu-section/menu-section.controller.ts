import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';

import { MenuSectionService } from './menu-section.service';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { RoleGuard } from '../auth/guards/role.guard';
import { Role } from '../../shared/decorators/role.decorator';
import { Roles } from '../../shared/enums/user-roles';
import { CreateMenuSectionDto } from './dto/create-menu-section.dto';
import { UpdateMenuSectionDto } from './dto/update-menu-section.dto';

@Controller('menu-section')
export class MenuSectionController {
  constructor(private menuSectionService: MenuSectionService) {}

  @Get(':id')
  get(@Param('id', ParseIntPipe) id: number) {
    return this.menuSectionService.getByWithRelations(
      { key: 'id', value: id },
      ['menu', 'dishes'],
    );
  }

  @Get()
  getAll() {
    return this.menuSectionService.getAll();
  }

  @Post()
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  create(@Body() createMenuSectionDto: CreateMenuSectionDto) {
    return this.menuSectionService.create(createMenuSectionDto);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateSectionDto: UpdateMenuSectionDto,
  ) {
    return this.menuSectionService.update(id, updateSectionDto);
  }
}
