import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MenuSection } from '../../entities/menu-section.entity';
import { MenuSectionService } from './menu-section.service';
import { MenuSectionController } from './menu-section.controller';
import { FileModule } from '../file/file.module';
import { MenuModule } from '../menu/menu.module';
import { DishModule } from '../dish/dish.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MenuSection]),
    FileModule,
    forwardRef(() => MenuModule),
    forwardRef(() => DishModule),
  ],
  providers: [MenuSectionService],
  controllers: [MenuSectionController],
  exports: [MenuSectionService],
})
export class MenuSectionModule {}
