import { Repository } from 'typeorm';
import {
  forwardRef,
  Inject,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { BaseService } from '../../shared/lib/base.service';
import { MenuSection } from '../../entities/menu-section.entity';
import { CreateMenuSectionDto } from './dto/create-menu-section.dto';
import { alreadyExist } from '../../shared/lib/functions';
import { FileService } from '../file/file.service';
import { MenuService } from '../menu/menu.service';
import { UpdateMenuSectionDto } from './dto/update-menu-section.dto';
import { EntityStatus } from '../../shared/enums/entity-status';
import { DishService } from '../dish/dish.service';

@Injectable()
export class MenuSectionService extends BaseService<MenuSection> {
  constructor(
    @InjectRepository(MenuSection)
    private menuSectionRepository: Repository<MenuSection>,
    private fileService: FileService,
    @Inject(forwardRef(() => MenuService))
    private menuService: MenuService,
    @Inject(forwardRef(() => DishService))
    private dishService: DishService,
  ) {
    super(menuSectionRepository, 'menuSection');
  }

  async getAll() {
    return this.menuSectionRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  async getByMenuId(id: number) {
    return this.menuSectionRepository
      .createQueryBuilder('menuSection')
      .leftJoinAndSelect('menuSection.dishes', 'dish')
      .leftJoinAndSelect('dish.file', 'file')
      .leftJoin('menuSection.menu', 'menu')
      .where('menu.id = :id', { id })
      .getMany();
  }

  async create(createMenuSectionDto: CreateMenuSectionDto) {
    const existMenuSection = await this.getBy({
      key: 'title',
      value: createMenuSectionDto.title,
    });
    if (existMenuSection)
      throw new UnprocessableEntityException([alreadyExist('menu section')]);

    const dishes = [];
    if (createMenuSectionDto.dishesIds) {
      for (const dishId of createMenuSectionDto.dishesIds) {
        const dish = await this.checkExist(dishId);
        dishes.push(dish);
      }
    }
    delete createMenuSectionDto.dishesIds;

    const menu = await this.menuService.checkExist(createMenuSectionDto.menuId);
    delete createMenuSectionDto.menuId;

    const menuSection = this.menuSectionRepository.create({
      ...createMenuSectionDto,
      dishes,
      menu,
    });
    return this.menuSectionRepository.save(menuSection);
  }

  async update(id: number, updateMenuSectionDto: UpdateMenuSectionDto) {
    const existSection = await this.checkExist(id);
    const updateData = updateMenuSectionDto;

    if (updateData.dishesIds) {
      const dishes = [];
      for (const dishId of updateData.dishesIds) {
        const dish = await this.dishService.checkExist(dishId);
        dishes.push(dish);
      }
      updateData['dishes'] = dishes;
      delete updateData.dishesIds;
    }

    if (updateData.menuId) {
      const menu = await this.menuService.checkExist(updateData.menuId);
      delete updateData.menuId;
      updateData['menu'] = menu;
    }

    const menuSection = this.menuSectionRepository.merge(
      existSection,
      updateData,
    );
    await this.menuSectionRepository.save(menuSection);

    return this.getByWithRelations({ key: 'id', value: id }, [
      'menu',
      'dishes',
    ]);
  }
}
