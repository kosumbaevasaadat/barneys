import { IsNumber, IsOptional } from 'class-validator';

export class UpdateMenuDto {
  @IsNumber()
  @IsOptional()
  imageId?: number;

  @IsNumber({}, { each: true })
  @IsOptional()
  sectionIds?: number[];
}
