import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class CreateMenuDto {
  @IsNumber()
  @IsNotEmpty()
  imageId: number;

  @IsNumber()
  @IsOptional()
  sectionIds?: number[];
}
