import { Repository } from 'typeorm';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { BaseService } from '../../shared/lib/base.service';
import { Menu } from '../../entities/menu.entity';
import { CreateMenuDto } from './dto/create-menu.dto';
import { FileService } from '../file/file.service';
import { MenuSectionService } from '../menu-section/menu-section.service';
import { EntityStatus } from '../../shared/enums/entity-status';
import { UpdateMenuDto } from './dto/update-menu.dto';

@Injectable()
export class MenuService extends BaseService<Menu> {
  constructor(
    @InjectRepository(Menu)
    private menuRepository: Repository<Menu>,
    private fileService: FileService,
    @Inject(forwardRef(() => MenuSectionService))
    private sectionService: MenuSectionService,
  ) {
    super(menuRepository, 'menu');
  }

  async getAll() {
    return this.menuRepository.find({ where: { status: EntityStatus.ACTIVE } });
  }

  async getOne(id: number) {
    const menu = await this.menuRepository
      .createQueryBuilder('menu')
      .where('menu.id = :id', { id })
      .leftJoinAndSelect('menu.file', 'file')
      .getOne();

    const sections = await this.sectionService.getByMenuId(id);
    menu.sections = sections;

    return menu;
  }

  async create(createMenuDto: CreateMenuDto) {
    const file = await this.fileService.checkExist(createMenuDto.imageId);
    delete createMenuDto.imageId;

    const sections = [];
    if (createMenuDto.sectionIds) {
      for (const sectionId of createMenuDto.sectionIds) {
        const section = await this.checkExist(sectionId);
        sections.push(section);
      }
    }

    const menu = this.menuRepository.create({ file, sections });
    return this.menuRepository.save(menu);
  }

  async update(id: number, updateMenuDto: UpdateMenuDto) {
    const existMenu = await this.checkExist(id);
    const updateData = {};

    if (updateMenuDto.sectionIds) {
      const sections = [];
      for (const sectionId of updateMenuDto.sectionIds) {
        const section = await this.sectionService.checkExist(sectionId);
        sections.push(section);
      }
      updateData['sections'] = sections;
    }

    if (updateMenuDto.imageId) {
      const file = await this.fileService.checkExist(updateMenuDto.imageId);
      updateData['file'] = file;
    }

    const menu = this.menuRepository.merge(existMenu, updateData);
    await this.menuRepository.save(menu);

    return this.getByWithRelations({ key: 'id', value: id }, [
      'file',
      'sections',
    ]);
  }
}
