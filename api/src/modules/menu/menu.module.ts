import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Menu } from '../../entities/menu.entity';
import { MenuService } from './menu.service';
import { MenuController } from './menu.controller';
import { FileModule } from '../file/file.module';
import { MenuSectionModule } from '../menu-section/menu-section.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Menu]),
    FileModule,
    forwardRef(() => MenuSectionModule),
  ],
  providers: [MenuService],
  controllers: [MenuController],
  exports: [MenuService],
})
export class MenuModule {}
