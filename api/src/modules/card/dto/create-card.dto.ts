import {
  IsAlpha,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import { Type } from 'class-transformer';

import { IsExpDate } from '../../../shared/decorators/is-exp-date';
import { CustomLength } from '../../../shared/decorators/custom-length';

export class CreateCardDto {
  @IsAlpha()
  @IsNotEmpty()
  cardName: string;

  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  @CustomLength(16)
  cardNumber: number;

  @IsString()
  @IsNotEmpty()
  @IsExpDate()
  expDate: string;

  @IsInt()
  @IsNotEmpty()
  @CustomLength(3)
  cardCode: number;
}
