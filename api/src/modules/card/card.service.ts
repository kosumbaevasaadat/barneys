import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Card } from '../../entities/card.entity';
import { CreateCardDto } from './dto/create-card.dto';
import { User } from '../../entities/user.entity';
import { BaseService } from '../../shared/lib/base.service';
import { EntityStatus } from '../../shared/enums/entity-status';

@Injectable()
export class CardService extends BaseService<Card> {
  constructor(
    @InjectRepository(Card)
    private cardRepository: Repository<Card>,
  ) {
    super(cardRepository, 'card');
  }

  async create(user: User, createCardDto: CreateCardDto) {
    const existCard = await this.getByUserId(user.id);
    if (existCard) {
      const cardData = await this.cardRepository.merge(existCard, {
        ...createCardDto,
        status: EntityStatus.ACTIVE,
      });
      return this.cardRepository.save(cardData);
    }

    const card = this.cardRepository.create({ ...createCardDto, user });
    return this.cardRepository.save(card);
  }

  async getByUserId(id: number) {
    return await this.cardRepository
      .createQueryBuilder('card')
      .leftJoinAndSelect(`card.user`, 'user')
      .where('user.id LIKE :id', { id })
      .getOne();
  }

  async remove(userId: number) {
    const existCard = await this.getByUserId(userId);
    if (!existCard) return;

    const card = await this.cardRepository.merge(existCard, {
      status: EntityStatus.DELETED,
    });
    await this.cardRepository.save(card);
  }
}
