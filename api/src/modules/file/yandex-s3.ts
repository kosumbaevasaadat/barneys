import EasyYandexS3 from 'easy-yandex-s3';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import {
  ACCESS_KEY_ID,
  BUCKET_NAME,
  SECRET_ACCESS_KEY,
} from './file.constants';

@Injectable()
export class YandexS3 {
  private readonly s3;

  constructor(private configService: ConfigService) {
    this.s3 = new EasyYandexS3({
      auth: {
        accessKeyId: this.configService.get<string>(ACCESS_KEY_ID),
        secretAccessKey: this.configService.get<string>(SECRET_ACCESS_KEY),
      },
      Bucket: this.configService.get<string>(BUCKET_NAME),
      debug: false,
    });
  }

  async upload(buffer, directoryPath = '/', fileName: string): Promise<any> {
    const options: any = { buffer };
    options.name = fileName;

    const original = await this.s3.Upload(options, directoryPath);
    return original.key || original.Key;
  }

  async get(fileKey: any) {
    return (await this.s3.Download(fileKey)).data;
  }

  async remove(fileKey): Promise<void> {
    await this.s3.Remove(fileKey);
  }
}
