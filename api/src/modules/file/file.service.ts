import { YandexS3 } from './yandex-s3';
import { Repository } from 'typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { ERRORS, EXT } from '../../shared/constants';
import { File } from '../../entities/file.entity';
import { BaseService } from '../../shared/lib/base.service';
import { EntityStatus } from '../../shared/enums/entity-status';
import { deletedSuccessfully } from '../../shared/lib/functions';

@Injectable()
export class FileService extends BaseService<File> {
  constructor(
    @InjectRepository(File)
    private fileRepository: Repository<File>,
    private yandexS3: YandexS3,
  ) {
    super(fileRepository, 'file');
  }

  async getFileById(id: number) {
    const file = await this.checkExist(id);
    return this.yandexS3.get(file.fileKey);
  }

  async upload(file: Express.Multer.File, { directory = '/' }) {
    const ext = file.mimetype.split('/')[1];

    if (!EXT.includes(ext)) {
      throw new UnprocessableEntityException(ERRORS.FILE.fileExt);
    }

    const fileName = Date.now().toString().concat('.', ext);
    const fileKey = await this.yandexS3.upload(
      file.buffer,
      directory,
      fileName,
    );

    const fileData = this.fileRepository.create({
      name: fileName,
      fileKey,
      contentType: file.mimetype,
      contentSize: file.size,
    });

    return this.fileRepository.save(fileData);
  }

  async removeFileById(id: number) {
    const file = await this.checkExist(id);

    await this.fileRepository.merge(file, { status: EntityStatus.DELETED });
    await this.fileRepository.save(file);

    await this.yandexS3.remove(file.fileKey);
    return { message: deletedSuccessfully('file', id) };
  }
}
