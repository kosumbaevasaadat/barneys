import {
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';

import { FileService } from './file.service';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { RoleGuard } from '../auth/guards/role.guard';
import { Role } from '../../shared/decorators/role.decorator';
import { Roles } from '../../shared/enums/user-roles';
import { QueryParams } from '../../shared/types/query-params';

const cached = new Map();

@Controller('file')
export class FileController {
  constructor(private fileService: FileService) {}

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) id: number,
    @Res({ passthrough: true }) res: Response,
  ) {
    const fileData = await this.fileService.getFileById(id);
    cached.set(fileData.fileKey, { ...fileData, date: +new Date() });
    res.header('Content-Type', cached.get(fileData.fileKey).ContentType);
    res.send(cached.get(fileData.fileKey).Body);
  }

  @Post()
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  @UseInterceptors(FileInterceptor('file'))
  upload(
    @UploadedFile() file: Express.Multer.File,
    @Query() query: QueryParams,
  ) {
    return this.fileService.upload(file, query);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.fileService.removeFileById(id);
  }
}
