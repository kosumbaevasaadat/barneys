import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { File } from '../../entities/file.entity';
import { FileController } from './file.controller';
import { FileService } from './file.service';
import { YandexS3 } from './yandex-s3';

@Module({
  imports: [TypeOrmModule.forFeature([File]), ConfigModule],
  providers: [FileService, YandexS3],
  controllers: [FileController],
  exports: [FileService],
})
export class FileModule {}
