import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CartProductService } from './cart-product.service';
import { CartProductController } from './cart-product.controller';
import { CartProduct } from '../../entities/cart-product.entity';
import { DishModule } from '../dish/dish.module';
import { CartModule } from '../cart/cart.module';

@Module({
  imports: [TypeOrmModule.forFeature([CartProduct]), DishModule, CartModule],
  controllers: [CartProductController],
  providers: [CartProductService],
  exports: [CartProductService],
})
export class CartProductModule {}
