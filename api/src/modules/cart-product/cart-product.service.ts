import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateCartProductDto } from './dto/create-cart-product.dto';
import { UpdateCartProductDto } from './dto/update-cart-product.dto';
import { User } from '../../entities/user.entity';
import { DishService } from '../dish/dish.service';
import { CartProduct } from '../../entities/cart-product.entity';
import { BaseService } from '../../shared/lib/base.service';
import { CartService } from '../cart/cart.service';

@Injectable()
export class CartProductService extends BaseService<CartProduct> {
  constructor(
    @InjectRepository(CartProduct)
    private cartProductRepository: Repository<CartProduct>,
    private readonly productService: DishService,
    private readonly cartService: CartService,
  ) {
    super(cartProductRepository, 'cartProduct');
  }

  async create(createCartProductDto: CreateCartProductDto, user: User) {
    const existCart = await this.cartService.getByUserId(user.id);
    const existProduct = await this.productService.checkExist(
      createCartProductDto.productId,
    );

    const existCartProduct = await this.getByProductId(existProduct.id);
    if (existCartProduct)
      return this.update(existCartProduct.id, {
        quantity: createCartProductDto.quantity,
      });

    const cartProduct = this.cartProductRepository.create({
      product: existProduct,
      quantity: createCartProductDto.quantity,
      cart: existCart,
    });
    return this.cartProductRepository.save(cartProduct);
  }

  getByProductId(id: number) {
    return this.cartProductRepository
      .createQueryBuilder('cartProduct')
      .leftJoinAndSelect('cartProduct.product', 'product')
      .where('product.id = :id', { id })
      .getOne();
  }

  async update(id: number, updateCartProductDto: UpdateCartProductDto) {
    const existCartProduct = await this.checkExist(id);

    const cartProduct = this.cartProductRepository.merge(existCartProduct, {
      quantity: updateCartProductDto.quantity,
    });
    return this.cartProductRepository.save(cartProduct);
  }

  remove(id: number) {
    return this.cartProductRepository.delete({ id });
  }
}
