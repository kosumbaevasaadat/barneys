import { IsInt, IsNotEmpty, Max, Min } from 'class-validator';

export class CreateCartProductDto {
  @IsInt()
  @IsNotEmpty()
  productId: number;

  @IsInt()
  @IsNotEmpty()
  @Min(1)
  @Max(100)
  quantity: number;
}
