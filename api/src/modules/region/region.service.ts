import { Repository } from 'typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateRegionDto } from './dto/create-region.dto';
import { BaseService } from '../../shared/lib/base.service';
import { Region } from '../../entities/region.entity';
import { alreadyExist, deletedSuccessfully } from '../../shared/lib/functions';
import { EntityStatus } from '../../shared/enums/entity-status';

@Injectable()
export class RegionService extends BaseService<Region> {
  constructor(
    @InjectRepository(Region)
    private readonly regionRepository: Repository<Region>,
  ) {
    super(regionRepository, 'region');
  }

  async create(createRegionDto: CreateRegionDto) {
    const existRegion = await this.getBy({
      key: 'title',
      value: createRegionDto.title,
    });
    if (existRegion)
      throw new UnprocessableEntityException(alreadyExist('region'));

    const region = this.regionRepository.create(createRegionDto);
    return this.regionRepository.save(region);
  }

  findAll() {
    return this.regionRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  findOne(id: number) {
    return this.checkExist(id);
  }

  async remove(id: number) {
    const existRegion = await this.checkExist(id);

    const region = this.regionRepository.merge(existRegion, {
      status: EntityStatus.DELETED,
    });
    await this.regionRepository.save(region);

    return deletedSuccessfully('region', id);
  }
}
