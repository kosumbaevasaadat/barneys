import { Repository } from 'typeorm';
import {
  forwardRef,
  Inject,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Dish } from '../../entities/dish.entity';
import { BaseService } from '../../shared/lib/base.service';
import { CreateDishDto } from './dto/create-dish.dto';
import { alreadyExist } from '../../shared/lib/functions';
import { FileService } from '../file/file.service';
import { MenuSectionService } from '../menu-section/menu-section.service';
import { UpdateDishDto } from './dto/update-dish.dto';
import { EntityStatus } from '../../shared/enums/entity-status';
import { ERRORS } from '../../shared/constants';

@Injectable()
export class DishService extends BaseService<Dish> {
  constructor(
    @InjectRepository(Dish)
    private dishRepository: Repository<Dish>,
    private fileService: FileService,
    @Inject(forwardRef(() => MenuSectionService))
    private menuSectionService: MenuSectionService,
  ) {
    super(dishRepository, 'dish');
  }

  async getAll() {
    return this.dishRepository.find({ where: { status: EntityStatus.ACTIVE } });
  }

  async getByImageId(imageId: number) {
    return this.dishRepository
      .createQueryBuilder(this.entity)
      .leftJoinAndSelect('dish.file', 'file')
      .andWhere('file.id = :id', { id: imageId })
      .getOne();
  }

  async create(createDishDto: CreateDishDto) {
    const existDish = await this.getBy({
      key: 'title',
      value: createDishDto.title,
    });
    if (existDish)
      throw new UnprocessableEntityException([alreadyExist('dish')]);

    const existWithSameImage = await this.getByImageId(createDishDto.imageId);
    if (existWithSameImage)
      throw new UnprocessableEntityException([
        ERRORS.DISH.alreadyExistWithSuchPic,
      ]);

    const file = await this.fileService.checkExist(createDishDto.imageId);
    delete createDishDto.imageId;

    const menuSection = await this.menuSectionService.checkExist(
      createDishDto.menuSectionId,
    );
    delete createDishDto.menuSectionId;

    const dish = this.dishRepository.create({
      ...createDishDto,
      file,
      menuSection,
    });
    return this.dishRepository.save(dish);
  }

  async update(id: number, updateDishDto: UpdateDishDto) {
    const existDish = await this.checkExist(id);
    const updateData = updateDishDto;

    if (updateDishDto.imageId) {
      const image = await this.fileService.checkExist(updateDishDto.imageId);
      updateData['file'] = image;
      delete updateData.imageId;
    }

    if (updateDishDto.menuSectionId) {
      const menuSection = await this.menuSectionService.checkExist(
        updateDishDto.menuSectionId,
      );
      updateData['menuSection'] = menuSection;
      delete updateData.menuSectionId;
    }

    const dish = this.dishRepository.merge(existDish, updateData);
    await this.dishRepository.save(dish);

    return this.getByWithRelations({ key: 'id', value: id }, [
      'file',
      'menuSection',
    ]);
  }
}
