import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Dish } from '../../entities/dish.entity';
import { DishService } from './dish.service';
import { DishController } from './dish.controller';
import { FileModule } from '../file/file.module';
import { MenuSectionModule } from '../menu-section/menu-section.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Dish]),
    FileModule,
    forwardRef(() => MenuSectionModule),
  ],
  providers: [DishService],
  controllers: [DishController],
  exports: [DishService],
})
export class DishModule {}
