import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ChainService } from './chain.service';
import { ChainController } from './chain.controller';
import { Chain } from '../../entities/chain.entity';
import { FileModule } from '../file/file.module';
import { MenuModule } from '../menu/menu.module';
import { LocationAddressModule } from '../location-address/location-address.module';
import { PromotionModule } from '../promotion/promotion.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Chain]),
    FileModule,
    MenuModule,
    LocationAddressModule,
    PromotionModule,
  ],
  controllers: [ChainController],
  providers: [ChainService],
})
export class ChainModule {}
