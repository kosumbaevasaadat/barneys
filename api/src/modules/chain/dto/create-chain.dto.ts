import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';

export class CreateChainDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsOptional()
  @MaxLength(500)
  quote?: string = null;

  @IsString()
  @IsNotEmpty()
  @MaxLength(2000)
  story: string;

  @IsInt()
  @IsNotEmpty()
  mainImageId: number;

  @IsInt()
  @IsNotEmpty()
  menuId: number;

  @IsInt()
  @IsNotEmpty()
  addressId: number;

  @IsArray()
  @IsOptional()
  promotionIds?: number[];
}
