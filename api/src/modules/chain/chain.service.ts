import { Repository } from 'typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateChainDto } from './dto/create-chain.dto';
import { BaseService } from '../../shared/lib/base.service';
import { Chain } from '../../entities/chain.entity';
import { alreadyExist, deletedSuccessfully } from '../../shared/lib/functions';
import { FileService } from '../file/file.service';
import { MenuService } from '../menu/menu.service';
import { EntityStatus } from '../../shared/enums/entity-status';
import { LocationAddressService } from '../location-address/location-address.service';
import { PromotionService } from '../promotion/promotion.service';

@Injectable()
export class ChainService extends BaseService<Chain> {
  constructor(
    @InjectRepository(Chain)
    private readonly chainRepository: Repository<Chain>,
    private readonly fileService: FileService,
    private readonly menuService: MenuService,
    private readonly locationAddressService: LocationAddressService,
    private readonly promotionService: PromotionService,
  ) {
    super(chainRepository, 'chain');
  }

  async create(createChainDto: CreateChainDto) {
    const existChain = await this.getBy({
      key: 'title',
      value: createChainDto.title,
    });
    if (existChain)
      throw new UnprocessableEntityException(alreadyExist('chain'));

    const chainData = createChainDto;

    const mainImage = await this.fileService.checkExist(
      createChainDto.mainImageId,
    );
    chainData['mainImage'] = mainImage;

    const menu = await this.menuService.checkExist(createChainDto.menuId);
    chainData['menu'] = menu;

    const address = await this.locationAddressService.checkExist(
      createChainDto.addressId,
    );
    chainData['address'] = address;

    const promotions = [];
    if (createChainDto.promotionIds.length) {
      for (const id of createChainDto.promotionIds) {
        const promotion = await this.promotionService.getBy({
          key: 'id',
          value: id,
        });
        if (promotion) {
          promotions.push(promotion);
        }
      }
      if (promotions.length) {
        chainData['promotions'] = promotions;
      }
    }

    const chain = this.chainRepository.create(chainData);
    return this.chainRepository.save(chain);
  }

  findAll() {
    return this.chainRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  findOne(id: number) {
    return this.checkExist(id);
  }

  async remove(id: number) {
    const existChain = await this.checkExist(id);

    const chain = this.chainRepository.merge(existChain, {
      status: EntityStatus.DELETED,
    });
    await this.chainRepository.save(chain);

    return deletedSuccessfully('chain', id);
  }
}
