import { PartialType } from '@nestjs/mapped-types';
import { CreateReceiveMethodDto } from './create-receive-method.dto';

export class UpdateReceiveMethodDto extends PartialType(
  CreateReceiveMethodDto,
) {}
