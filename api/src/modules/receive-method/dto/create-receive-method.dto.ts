import { IsNotEmpty, IsString } from 'class-validator';

export class CreateReceiveMethodDto {
  @IsNotEmpty()
  @IsString()
  title: string;
}
