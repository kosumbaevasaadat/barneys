import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';

import { ReceiveMethodService } from './receive-method.service';
import { CreateReceiveMethodDto } from './dto/create-receive-method.dto';
import { UpdateReceiveMethodDto } from './dto/update-receive-method.dto';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { RoleGuard } from '../auth/guards/role.guard';
import { Role } from '../../shared/decorators/role.decorator';
import { Roles } from '../../shared/enums/user-roles';

@Controller('receive-method')
export class ReceiveMethodController {
  constructor(private readonly receiveMethodService: ReceiveMethodService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  create(@Body() createReceiveMethodDto: CreateReceiveMethodDto) {
    return this.receiveMethodService.create(createReceiveMethodDto);
  }

  @Get()
  findAll() {
    return this.receiveMethodService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.receiveMethodService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateReceiveMethodDto: UpdateReceiveMethodDto,
  ) {
    return this.receiveMethodService.update(id, updateReceiveMethodDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Role([Roles.ADMIN])
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.receiveMethodService.remove(id);
  }
}
