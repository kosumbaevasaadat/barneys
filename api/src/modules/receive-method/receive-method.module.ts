import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ReceiveMethodService } from './receive-method.service';
import { ReceiveMethodController } from './receive-method.controller';
import { ReceiveMethod } from '../../entities/receive-method.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ReceiveMethod])],
  controllers: [ReceiveMethodController],
  providers: [ReceiveMethodService],
  exports: [ReceiveMethodService],
})
export class ReceiveMethodModule {}
