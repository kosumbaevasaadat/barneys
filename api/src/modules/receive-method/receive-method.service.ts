import { Repository } from 'typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateReceiveMethodDto } from './dto/create-receive-method.dto';
import { UpdateReceiveMethodDto } from './dto/update-receive-method.dto';
import { ReceiveMethod } from '../../entities/receive-method.entity';
import { BaseService } from '../../shared/lib/base.service';
import { alreadyExist } from '../../shared/lib/functions';
import { EntityStatus } from '../../shared/enums/entity-status';

@Injectable()
export class ReceiveMethodService extends BaseService<ReceiveMethod> {
  constructor(
    @InjectRepository(ReceiveMethod)
    private receiveMethodRepository: Repository<ReceiveMethod>,
  ) {
    super(receiveMethodRepository, 'receiveMethod');
  }

  async create(createReceiveMethodDto: CreateReceiveMethodDto) {
    const modifiedTitle = createReceiveMethodDto.title.trim().toLowerCase();

    const existReceiveMethod = await this.getBy({
      key: 'title',
      value: modifiedTitle,
    });
    if (existReceiveMethod)
      throw new UnprocessableEntityException(alreadyExist('receive method'));

    const receiveMethod = this.receiveMethodRepository.create(
      createReceiveMethodDto,
    );
    return this.receiveMethodRepository.save(receiveMethod);
  }

  findAll() {
    return this.receiveMethodRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  findOne(id: number) {
    return this.getBy({ key: 'id', value: id });
  }

  async update(id: number, updateReceiveMethodDto: UpdateReceiveMethodDto) {
    const existReceiveMethod = await this.checkExist(id);

    const receiveMethod = await this.receiveMethodRepository.merge(
      existReceiveMethod,
      updateReceiveMethodDto,
    );
    return this.receiveMethodRepository.save(receiveMethod);
  }

  async remove(id: number) {
    const existReceiveMethod = await this.checkExist(id);

    const receiveMethod = await this.receiveMethodRepository.merge(
      existReceiveMethod,
      { status: EntityStatus.DELETED },
    );
    return this.receiveMethodRepository.save(receiveMethod);
  }
}
