import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { State } from '../../entities/state.entity';
import { StateService } from './state.service';
import { StateController } from './state.contoller';
import { LocationAddressModule } from '../location-address/location-address.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([State]),
    forwardRef(() => LocationAddressModule),
  ],
  providers: [StateService],
  controllers: [StateController],
  exports: [StateService],
})
export class StateModule {}
