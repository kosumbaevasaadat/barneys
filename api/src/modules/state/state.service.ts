import { Repository } from 'typeorm';
import {
  forwardRef,
  Inject,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { BaseService } from '../../shared/lib/base.service';
import { State } from '../../entities/state.entity';
import { CreateStateDto } from './dto/create-state.dto';
import {
  alreadyExist,
  deletedSuccessfully,
  notExist,
  sameExist,
} from '../../shared/lib/functions';
import { UpdateCityDto } from '../city/dto/update-city.dto';
import { EntityStatus } from '../../shared/enums/entity-status';
import { LocationAddressService } from '../location-address/location-address.service';

@Injectable()
export class StateService extends BaseService<State> {
  constructor(
    @InjectRepository(State)
    private stateRepository: Repository<State>,
    @Inject(forwardRef(() => LocationAddressService))
    private locationAddressService: LocationAddressService,
  ) {
    super(stateRepository, 'state');
  }

  async get() {
    return this.stateRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  async create(createStateDto: CreateStateDto) {
    const existState = await this.getBy({
      key: 'title',
      value: createStateDto.title,
    });
    if (existState)
      throw new UnprocessableEntityException([alreadyExist('state')]);

    const state = this.stateRepository.create(createStateDto);
    return this.stateRepository.save(state);
  }

  async update(id: number, updateCityDto: UpdateCityDto) {
    const existState = await this.getBy({ key: 'id', value: id });
    if (!existState)
      throw new UnprocessableEntityException([notExist('state')]);

    const sameState = await this.getBy({
      key: 'title',
      value: updateCityDto.title,
    });
    if (sameState) throw new UnprocessableEntityException([sameExist('state')]);

    const state = this.stateRepository.merge(existState, updateCityDto);
    return this.stateRepository.save(state);
  }

  async remove(id: number) {
    const existState = await this.getBy({ key: 'id', value: id });
    if (!existState)
      throw new UnprocessableEntityException([notExist('state')]);

    const state = this.stateRepository.merge(existState, {
      status: EntityStatus.DELETED,
    });
    await this.locationAddressService.updateManyByState(id);
    await this.repository.save(state);

    return { message: deletedSuccessfully('state', id) };
  }
}
