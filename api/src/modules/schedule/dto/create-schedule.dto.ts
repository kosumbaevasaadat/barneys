import { IsInt, IsNotEmpty, IsString } from 'class-validator';

import { IsWeekdays } from '../../../shared/decorators/is-weekdays';
import { IsHours } from '../../../shared/decorators/is-hours';

export class CreateScheduleDto {
  @IsString()
  @IsNotEmpty()
  @IsWeekdays()
  days: string;

  @IsString()
  @IsNotEmpty()
  @IsHours()
  hours: string;

  @IsNotEmpty()
  @IsInt()
  branchId: number;
}
