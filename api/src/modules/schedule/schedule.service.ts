import { Repository } from 'typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateScheduleDto } from './dto/create-schedule.dto';
import { UpdateScheduleDto } from './dto/update-schedule.dto';
import { BaseService } from '../../shared/lib/base.service';
import { Schedule } from '../../entities/schedule.entity';
import { alreadyExist } from '../../shared/lib/functions';
import { EntityStatus } from '../../shared/enums/entity-status';

@Injectable()
export class ScheduleService extends BaseService<Schedule> {
  constructor(
    @InjectRepository(Schedule)
    private scheduleRepository: Repository<Schedule>,
  ) {
    super(scheduleRepository, 'schedule');
  }

  create(createScheduleDto: CreateScheduleDto) {
    const existSchedule = this.scheduleRepository
      .createQueryBuilder('schedule')
      .where(`schedule.days = :days`, { days: createScheduleDto.days })
      .andWhere(`schedule.hours = :hours`, { hours: createScheduleDto.hours })
      .leftJoinAndSelect(`schedule.branch`, 'branch')
      .andWhere('branch.id = :id', { id: createScheduleDto.branchId });

    if (existSchedule)
      throw new UnprocessableEntityException(alreadyExist('branch'));

    const schedule = this.scheduleRepository.create(createScheduleDto);
    return this.scheduleRepository.save(schedule);
  }

  findAll() {
    return this.scheduleRepository.find({
      where: { status: EntityStatus.ACTIVE },
    });
  }

  findOne(id: number) {
    return this.getBy({ key: 'id', value: id });
  }

  async update(id: number, updateScheduleDto: UpdateScheduleDto) {
    const existSchedule = await this.checkExist(id);

    const schedule = this.scheduleRepository.merge(
      existSchedule,
      updateScheduleDto,
    );
    return this.scheduleRepository.save(schedule);
  }

  async remove(id: number) {
    const existSchedule = await this.checkExist(id);

    const schedule = this.scheduleRepository.merge(existSchedule, {
      status: EntityStatus.DELETED,
    });
    return this.scheduleRepository.save(schedule);
  }
}
