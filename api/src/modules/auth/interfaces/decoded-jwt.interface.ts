import { Roles } from 'src/shared/enums/user-roles';

export interface IDecodedJwt {
  id: number;
  type: string;
  role: Roles;
}
