import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ERRORS } from '../../../shared/constants';
import { TOKEN_TYPES } from '../auth.constants';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  handleRequest(err, user, info) {
    if (err || !user) {
      throw err || new UnauthorizedException(info.message);
    }

    if (user.type !== TOKEN_TYPES.accessToken) {
      throw err || new UnauthorizedException(ERRORS.AUTH.noAccessToken);
    }

    return user;
  }
}
