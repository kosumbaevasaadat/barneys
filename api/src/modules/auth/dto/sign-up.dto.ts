import { IsString, MinLength } from 'class-validator';

import { AuthDto } from './auth.dto';
import { IsPhoneNumber } from '../../../shared/decorators/is-phone-number';

export class SignUpDto extends AuthDto {
  @MinLength(1)
  @IsString()
  firstName: string;

  @MinLength(1)
  @IsString()
  lastName: string;

  @IsString()
  @IsPhoneNumber()
  phoneNumber: string;
}
