import { IsEmail, IsString } from 'class-validator';
import { IsPassword } from '../../../shared/decorators/is-password';

export class AuthDto {
  @IsString()
  @IsEmail()
  email: string;

  @IsString()
  @IsPassword()
  password: string;
}
