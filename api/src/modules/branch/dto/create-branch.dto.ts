import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';
import { IsPhoneNumber } from '../../../shared/decorators/is-phone-number';

export class CreateBranchDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  description: string;

  @IsString()
  @IsNotEmpty()
  @IsPhoneNumber()
  phone: string;

  @IsString()
  @IsNotEmpty()
  @IsUrl()
  yelpUrl: string;

  @IsNotEmpty()
  @IsInt()
  addressId: number;

  @IsOptional()
  @IsInt()
  mainImageId?: number;

  @IsOptional()
  @IsInt()
  subImageId?: number;

  @IsOptional()
  @IsInt()
  @IsArray()
  scheduleIds?: number[];

  @IsArray()
  @IsOptional()
  receiveMethodIds?: number[];
}
