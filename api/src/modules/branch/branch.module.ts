import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BranchService } from './branch.service';
import { BranchController } from './branch.controller';
import { Branch } from '../../entities/branch.entity';
import { FileModule } from '../file/file.module';
import { ScheduleModule } from '../schedule/schedule.module';
import { AddressModule } from '../address/address.module';
import { ReceiveMethodModule } from '../receive-method/receive-method.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Branch]),
    FileModule,
    AddressModule,
    ScheduleModule,
    ReceiveMethodModule,
  ],
  controllers: [BranchController],
  providers: [BranchService],
  exports: [BranchService],
})
export class BranchModule {}
