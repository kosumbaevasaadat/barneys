import { Repository } from 'typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { BaseService } from '../../shared/lib/base.service';
import { Branch } from '../../entities/branch.entity';
import { alreadyExist, notExist } from '../../shared/lib/functions';
import { FileService } from '../file/file.service';
import { AddressService } from '../address/address.service';
import { ScheduleService } from '../schedule/schedule.service';
import { ReceiveMethodService } from '../receive-method/receive-method.service';
import { EntityStatus } from '../../shared/enums/entity-status';

@Injectable()
export class BranchService extends BaseService<Branch> {
  constructor(
    @InjectRepository(Branch)
    private readonly branchRepository: Repository<Branch>,
    private readonly fileService: FileService,
    private readonly addressService: AddressService,
    private readonly scheduleService: ScheduleService,
    private readonly receiveMethodService: ReceiveMethodService,
  ) {
    super(branchRepository, 'branch');
  }

  async create(createBranchDto: CreateBranchDto) {
    const existBranch = await this.getBy({
      key: 'title',
      value: createBranchDto.title,
    });
    if (existBranch)
      throw new UnprocessableEntityException(alreadyExist('branch'));

    const createData = createBranchDto;

    if (createBranchDto.mainImageId) {
      const mainImage = await this.fileService.getFileById(
        createBranchDto.mainImageId,
      );
      //проверить что возвращается в mainImage, чтобы прокинуть ошибку
      if (!mainImage)
        throw new UnprocessableEntityException(notExist('main image'));

      createData['mainImage'] = mainImage;
      delete createData.mainImageId;
    }

    if (createBranchDto.subImageId) {
      const subImage = await this.fileService.getFileById(
        createBranchDto.subImageId,
      );
      //проверить что возвращается в mainImage, чтобы прокинуть ошибку
      if (!subImage)
        throw new UnprocessableEntityException(notExist('sub image'));

      createData['subImage'] = subImage;
      delete createData.subImageId;
    }

    const address = await this.addressService.checkExist(
      createBranchDto.addressId,
    );
    createData['address'] = address;
    delete createData.subImageId;

    if (createBranchDto.scheduleIds) {
      const schedules = [];
      for (const id of createBranchDto.scheduleIds) {
        const schedule = await this.scheduleService.checkExist(id);
        schedules.push(schedule);
      }
      createData['schedules'] = schedules;
    }

    if (createBranchDto.receiveMethodIds) {
      const receiveMethods = [];
      for (const id of createBranchDto.receiveMethodIds) {
        const receiveMethod = await this.receiveMethodService.checkExist(id);
        receiveMethods.push(receiveMethod);
      }
      createData['receiveMethods'] = receiveMethods;
    }

    const branch = this.branchRepository.create(createData);
    return this.branchRepository.save(branch);
  }

  findAll() {
    return this.branchRepository
      .createQueryBuilder('branch')
      .where('branch.status = :status', { status: EntityStatus.ACTIVE })
      .leftJoinAndSelect('branch.receiveMethods', 'receiveMethods')
      .leftJoinAndSelect('branch.address', 'address')
      .leftJoinAndSelect('address.region', 'region')
      .leftJoinAndSelect('address.city', 'city')
      .leftJoinAndSelect('address.state', 'state')
      .getMany();
  }

  findOne(id: number) {
    return this.branchRepository
      .createQueryBuilder('branch')
      .where('branch.id = :id', { id })
      .andWhere('branch.status = :status', { status: EntityStatus.ACTIVE })
      .leftJoinAndSelect('branch.schedules', 'schedule')
      .leftJoinAndSelect('branch.mainImage', 'file')
      .leftJoinAndSelect('branch.subImage', 'file2')
      .leftJoinAndSelect('branch.address', 'address')
      .leftJoinAndSelect('address.city', 'city')
      .leftJoinAndSelect('address.state', 'state')
      .getOne();
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    const existBranch = await this.checkExist(id);

    const updateData = updateBranchDto;

    if (updateBranchDto.mainImageId) {
      const mainImage = await this.fileService.getFileById(
        updateBranchDto.mainImageId,
      );
      //проверить что возвращается в mainImage, чтобы прокинуть ошибку
      if (!mainImage)
        throw new UnprocessableEntityException(notExist('main image'));

      updateData['mainImage'] = mainImage;
      delete updateData.mainImageId;
    }

    if (updateBranchDto.subImageId) {
      const subImage = await this.fileService.getFileById(
        updateBranchDto.subImageId,
      );
      //проверить что возвращается в mainImage, чтобы прокинуть ошибку
      if (!subImage)
        throw new UnprocessableEntityException(notExist('sub image'));

      updateData['subImage'] = subImage;
      delete updateData.subImageId;
    }

    if (updateBranchDto.addressId) {
      const address = await this.addressService.checkExist(
        updateBranchDto.addressId,
      );
      updateData['address'] = address;
      delete updateData.subImageId;
    }

    if (updateBranchDto.scheduleIds) {
      const schedules = [];
      for (const id of updateBranchDto.scheduleIds) {
        const schedule = await this.scheduleService.checkExist(id);
        schedules.push(schedule);
      }
      updateData['schedules'] = schedules;
    }

    if (updateBranchDto.receiveMethodIds) {
      const receiveMethods = [];
      for (const id of updateBranchDto.receiveMethodIds) {
        const receiveMethod = await this.receiveMethodService.checkExist(id);
        receiveMethods.push(receiveMethod);
      }
      updateData['receiveMethods'] = receiveMethods;
    }

    const branch = this.branchRepository.merge(existBranch, updateData);
    return this.branchRepository.save(branch);
  }

  async remove(id: number) {
    const existBranch = await this.checkExist(id);
    const updatedBranch = this.branchRepository.merge(existBranch, {
      status: EntityStatus.DELETED,
    });
    return this.branchRepository.save(updatedBranch);
  }
}
