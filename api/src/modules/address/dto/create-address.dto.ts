import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateAddressDto {
  @IsString()
  @IsNotEmpty()
  street: string;

  @IsNumber()
  @IsNotEmpty()
  apt: number;

  @IsNumber()
  @IsOptional()
  building: number = null;

  @IsNumber()
  @IsOptional()
  floor: number = null;
}
