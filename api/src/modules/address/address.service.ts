import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Address } from '../../entities/address.entity';
import { CreateAddressDto } from './dto/create-address.dto';
import { EntityStatus } from '../../shared/enums/entity-status';
import { BaseService } from '../../shared/lib/base.service';
import { UpdateAddressDto } from './dto/update-address.dto';

@Injectable()
export class AddressService extends BaseService<Address> {
  constructor(
    @InjectRepository(Address)
    private addressRepository: Repository<Address>,
  ) {
    super(addressRepository, 'address');
  }

  async create(createAddressDto: CreateAddressDto) {
    const address = this.addressRepository.create(createAddressDto);
    return this.addressRepository.save(address);
  }

  async update(id: number, updateAddressDto: UpdateAddressDto) {
    const existAddress = await this.checkExist(id);

    const updatedAddress = this.addressRepository.merge(
      existAddress,
      updateAddressDto,
    );
    return this.addressRepository.save(updatedAddress);
  }

  async remove(id: number) {
    const existAddress = await this.checkExist(id);
    if (!existAddress) return;

    const address = await this.addressRepository.merge(existAddress, {
      status: EntityStatus.DELETED,
    });
    await this.addressRepository.save(address);
  }
}
