import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { AddressModule } from './modules/address/address.module';
import { CardModule } from './modules/card/card.module';
import { CityModule } from './modules/city/city.module';
import { StateModule } from './modules/state/state.module';
import { LocationAddressModule } from './modules/location-address/location-address.module';
import { FileModule } from './modules/file/file.module';
import { DishModule } from './modules/dish/dish.module';
import { MenuSectionModule } from './modules/menu-section/menu-section.module';
import { MenuModule } from './modules/menu/menu.module';
import { ScheduleModule } from './modules/schedule/schedule.module';
import { ReceiveMethodModule } from './modules/receive-method/receive-method.module';
import { BranchModule } from './modules/branch/branch.module';
import { dataSourceOptions } from './db/data-source';
import { PromotionModule } from './modules/promotion/promotion.module';
import { ChainModule } from './modules/chain/chain.module';
import { RegionModule } from './modules/region/region.module';
import { CartProductModule } from './modules/cart-product/cart-product.module';
import { CartModule } from './modules/cart/cart.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(dataSourceOptions),
    UserModule,
    AuthModule,
    AddressModule,
    CardModule,
    CityModule,
    StateModule,
    LocationAddressModule,
    FileModule,
    DishModule,
    MenuSectionModule,
    MenuModule,
    ScheduleModule,
    ReceiveMethodModule,
    BranchModule,
    PromotionModule,
    ChainModule,
    RegionModule,
    CartProductModule,
    CartModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
