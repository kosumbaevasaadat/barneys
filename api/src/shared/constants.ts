export const ERRORS = {
  USER: {
    alreadyExist: 'User already exist',
    notExist: 'User does not exist',
    emailExist: 'User with such email already exist!',
  },
  AUTH: {
    password:
      'The password must contain from 8 to 64 characters, must include 2 letters (at least 1 uppercase and one lowercase), numbers and special characters!',
    wrongCredentials: 'Wrong credentials provided',
    noRefreshToken: 'No refresh token',
    invalidRefreshToken: 'Refresh token is not valid',
    forbiddenResource: 'Forbidden resource',
    accountDeleted: 'Your account was deleted',
    phoneNumberFormatError: 'Incorrect phone number format',
    noAccessToken: 'No access token!',
  },
  GENERAL: {
    length: 'Incorrect length',
    expDate: 'Incorrect format of expiration date!',
    alreadyExist: 'already exist!',
    notExist: "doesn't exist!",
    incorrectType: 'The data type must be string',
    sameExist: 'Already exists a similar',
    notFound: 'Not found by id = ',
    cannotAccess: 'Cannot access to the data',
    weekdays:
      'The days of the week should correspond to the format "Day of the week" + " " + "- or &" + " " + "Day of the week"',
    hours:
      'Opening hours must correspond to the format "hour" + "AM/PM " + " " + "-" + " " + "hour" + "AM/PM"',
  },
  ADDRESS: {
    streetNumFirstSymbol: 'The first character should be #',
  },
  FILE: {
    fileExt: 'Incorrect file extension',
  },
  DISH: {
    alreadyExistWithSuchPic: 'A dish with such a picture already exists!',
  },
};

export const MESSAGES = {
  AUTH: {
    logoutSuccess: 'You have been successfully logged out!',
  },
};

export const USER = {
  id: 'id',
  email: 'email',
};

export const REGEX = {
  password: /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&]).{8,64}$/,
  phone: /^\+1( )?\(?\d{3}\) \d{3}-\d{4}$/,
  expDate: /^(0[1-9]|1[0-2])\/?(\d{2})$/,
  weekDays:
    /^(Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday) [-&] (Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday)$/,
  hours:
    /^(1[0-2]|0?[1-9])(:[0-5][0-9])?(AM|PM)?(AM|PM) - (1[0-2]|0?[1-9])(:[0-5][0-9])?(AM|PM)?(AM|PM)$/,
};

export const EXT = ['jpeg', 'jpg', 'png', 'gif', 'svg', 'webp', 'avif'];
