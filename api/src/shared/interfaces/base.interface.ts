import { EntityStatus } from '../enums/entity-status';

export interface BaseInterface {
  id: number;
  status: EntityStatus;
  createDate: Date;
  updateDate: Date;
}
