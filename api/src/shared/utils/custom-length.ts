export const customLength = (
  number: number | string,
  length: number,
): boolean => number.toString().length === length;
