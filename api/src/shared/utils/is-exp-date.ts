import { REGEX } from '../constants';

export const isExpDate = (expDate: string): boolean =>
  REGEX.expDate.test(expDate);
