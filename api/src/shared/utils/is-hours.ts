import { REGEX } from '../constants';

export const isHours = (hours: string): boolean => REGEX.hours.test(hours);
