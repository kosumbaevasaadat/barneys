import { REGEX } from '../constants';

export const isWeekdays = (weekdays: string) => REGEX.weekDays.test(weekdays);
