import { REGEX } from '../constants';

export const isPhoneNumber = (login) => REGEX.phone.test(login);
