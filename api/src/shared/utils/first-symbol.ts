import { UnprocessableEntityException } from '@nestjs/common';
import { ERRORS } from '../constants';

export const firstSymbol = (data: any, symbol: string): boolean => {
  if (typeof data !== 'string')
    throw new UnprocessableEntityException([ERRORS.GENERAL.incorrectType]);
  return data.charAt(0) === symbol;
};
