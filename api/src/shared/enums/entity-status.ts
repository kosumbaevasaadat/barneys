export enum EntityStatus {
  ACTIVE = 'ACTIVE',
  DELETED = 'DELETED',
}

export enum ProceedStatus {
  ACTIVE = 'ACTIVE',
  PENDING = 'PENDING',
  PAID = 'PAID',
}
