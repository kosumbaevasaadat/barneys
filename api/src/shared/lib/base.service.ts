import { ObjectLiteral, Repository } from 'typeorm';
import { BadRequestException, NotFoundException } from '@nestjs/common';

import { GetByDto } from '../dto/get-by';
import { EntityStatus } from '../enums/entity-status';
import { ERRORS } from '../constants';

export class BaseService<Entity extends ObjectLiteral> {
  constructor(public repository: Repository<Entity>, public entity: string) {}

  async getBy({ key, value }: GetByDto, deleted?: boolean) {
    const query = this.repository
      .createQueryBuilder(this.entity)
      .where(`${this.entity}.${key} = :${key}`, { [key]: value })
      .andWhere(`${this.entity}.status = :status`, {
        status: EntityStatus.ACTIVE,
      });

    if (deleted === true) {
      query.andWhere(`${this.entity}.status = :status`, {
        status: EntityStatus.DELETED,
      });
    }

    return query.getOne();
  }

  async getByMultipleProperties(getByDtos: GetByDto[]) {
    const query = this.repository.createQueryBuilder(this.entity);

    getByDtos.forEach((el) => {
      query.andWhere(`${this.entity}.${el.key} = :${el.key}`, {
        [el.key]: el.value,
      });
    });

    return query.getOne();
  }

  async getByWithRelations({ key, value }: GetByDto, relations: string[]) {
    const query = this.repository
      .createQueryBuilder(this.entity)
      .where(`${this.entity}.${key} = :${key}`, { [key]: value });

    relations.forEach((relation) => {
      query.leftJoinAndSelect(`${this.entity}.${relation}`, relation);
    });

    return query.getOne();
  }

  async checkExist(id: number) {
    const data = await this.getBy({ key: 'id', value: id });

    if (!data) {
      throw new NotFoundException(ERRORS.GENERAL.notFound + id);
    }

    if (data.status !== EntityStatus.ACTIVE) {
      throw new BadRequestException(ERRORS.GENERAL.cannotAccess);
    }

    return data;
  }
}
