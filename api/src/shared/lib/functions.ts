import { ERRORS } from '../constants';

export const capitalizeFirstChar = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const alreadyExist = (entity: string) => {
  const capitalized = capitalizeFirstChar(entity);
  return `${capitalized} ${ERRORS.GENERAL.alreadyExist}`;
};

export const notExist = (entity: string) => {
  const capitalized = capitalizeFirstChar(entity);
  return `${capitalized} ${ERRORS.GENERAL.notExist}`;
};

export const sameExist = (entity: string) => {
  return `${ERRORS.GENERAL.sameExist} ${entity}`;
};

export const deletedSuccessfully = (entity: string, id: number) => {
  const capitalized = capitalizeFirstChar(entity);
  return `${capitalized} by id = ${id} successfully deleted!`;
};

export const arrayErr = (entity: string) => {
  return `The passed array of ${entity} ids contains non-existent data`;
};
