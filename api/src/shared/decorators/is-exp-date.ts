import { registerDecorator, ValidationOptions } from 'class-validator';

import { ERRORS } from '../constants';
import { isExpDate } from '../utils/is-exp-date';

export const IsExpDate = (validationOptions?: ValidationOptions) => {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'isExpDate',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any): boolean {
          return isExpDate(value);
        },
        defaultMessage: () => ERRORS.GENERAL.expDate,
      },
    });
  };
};
