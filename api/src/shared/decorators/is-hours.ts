import { registerDecorator, ValidationOptions } from 'class-validator';

import { ERRORS } from '../constants';
import { isHours } from '../utils/is-hours';

export const IsHours = (validationOptions?: ValidationOptions) => {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'isHours',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any): boolean {
          return isHours(value);
        },
        defaultMessage: () => ERRORS.GENERAL.hours,
      },
    });
  };
};
