import { registerDecorator, ValidationOptions } from 'class-validator';

import { ERRORS } from '../constants';
import { isWeekdays } from '../utils/is-weekdays';

export const IsWeekdays = (validationOptions?: ValidationOptions) => {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'isWeekdays',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any): boolean {
          return isWeekdays(value);
        },
        defaultMessage: () => ERRORS.GENERAL.weekdays,
      },
    });
  };
};
