import { registerDecorator } from 'class-validator';
import { ERRORS } from '../constants';
import { customLength } from '../utils/custom-length';

export const CustomLength = (length: number) => {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'customLength',
      target: object.constructor,
      propertyName: propertyName,
      validator: {
        validate(value: number | string): boolean {
          return customLength(value, length);
        },
        defaultMessage: () => ERRORS.GENERAL.length,
      },
    });
  };
};
