import { registerDecorator } from 'class-validator';

import { firstSymbol } from '../utils/first-symbol';
import { ERRORS } from '../constants';

export const FirstSymbol = (symbol: any) => {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'firstSymbol',
      target: object.constructor,
      propertyName: propertyName,
      validator: {
        validate(value: any): boolean {
          return firstSymbol(value, symbol);
        },
        defaultMessage: () => ERRORS.ADDRESS.streetNumFirstSymbol,
      },
    });
  };
};
