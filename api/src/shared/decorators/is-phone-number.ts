import { registerDecorator, ValidationOptions } from 'class-validator';

import { isPhoneNumber } from '../utils/is-phone-number';
import { ERRORS } from '../constants';

export const IsPhoneNumber = (validationOptions?: ValidationOptions) => {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'isPhoneNumber',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(phoneNumber = '') {
          return isPhoneNumber(phoneNumber);
        },
        defaultMessage: () => ERRORS.AUTH.phoneNumberFormatError,
      },
    });
  };
};
