import { SetMetadata } from '@nestjs/common';
import { Roles } from '../enums/user-roles';

export const Role = (roles: Roles[]) => SetMetadata('roles', roles);
