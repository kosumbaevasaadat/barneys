import { Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { File } from './file.entity';
import { Chain } from './chain.entity';

@Entity()
export class Promotion extends BaseEntity {
  @OneToOne(() => File)
  @JoinColumn()
  image: File;

  @ManyToOne(() => Chain, (chain) => chain.promotions)
  chain: Chain;
}
