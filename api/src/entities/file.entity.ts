import { Column, Entity } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity()
export class File extends BaseEntity {
  @Column()
  name: string;

  @Column()
  fileKey: string;

  @Column({ type: 'int' })
  contentSize: number;

  @Column()
  contentType: string;
}
