import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Dish } from './dish.entity';
import { Cart } from './cart.entity';

@Entity()
export class CartProduct extends BaseEntity {
  @Column({ type: 'integer' })
  quantity: number;

  @ManyToOne(() => Dish)
  product: Dish;

  @ManyToOne(() => Cart, (cart) => cart.products)
  cart: Cart;
}
