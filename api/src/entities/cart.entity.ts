import { Column, Entity, OneToMany, OneToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { User } from './user.entity';
import { ProceedStatus } from '../shared/enums/entity-status';
import { CartProduct } from './cart-product.entity';

@Entity()
export class Cart extends BaseEntity {
  @Column({ default: ProceedStatus.ACTIVE })
  proceedStatus: ProceedStatus;

  @OneToOne(() => User, (user) => user.cart)
  user: User;

  @OneToMany(() => CartProduct, (cartProduct) => cartProduct.cart)
  products: CartProduct[];
}
