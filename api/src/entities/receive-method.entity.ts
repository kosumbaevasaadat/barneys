import { Column, Entity, ManyToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Branch } from './branch.entity';

@Entity()
export class ReceiveMethod extends BaseEntity {
  @Column({ unique: true })
  title: string;

  @ManyToMany(() => Branch, (branch) => branch.receiveMethods)
  branches: Branch[];
}
