import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { User } from './user.entity';

@Entity()
export class Card extends BaseEntity {
  @Column()
  cardName: string;

  @Column({ type: 'bigint' })
  cardNumber: number;

  @Column()
  expDate: string;

  @Column()
  cardCode: number;

  @OneToOne(() => User, (user) => user.card)
  @JoinColumn()
  user: User;
}
