import { Column, Entity, ManyToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { City } from './city.entity';
import { State } from './state.entity';
import { Region } from './region.entity';

@Entity()
export class LocationAddress extends BaseEntity {
  @Column()
  street: string;

  @Column({ type: 'integer' })
  building: number;

  @Column({ type: 'integer', nullable: true, default: null })
  unit: number;

  @Column({ type: 'integer' })
  zipCode: number;

  @ManyToOne(() => City, { nullable: true })
  city: City;

  @ManyToOne(() => State, { nullable: true })
  state: State;

  @ManyToOne(() => Region, { nullable: false })
  region: Region;
}
