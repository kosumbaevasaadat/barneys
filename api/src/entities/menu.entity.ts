import { Entity, JoinColumn, OneToMany, OneToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { MenuSection } from './menu-section.entity';
import { File } from './file.entity';

@Entity()
export class Menu extends BaseEntity {
  @OneToOne(() => File)
  @JoinColumn()
  file: File;

  @OneToMany(() => MenuSection, (menuSection) => menuSection.menu)
  sections: MenuSection[];
}
