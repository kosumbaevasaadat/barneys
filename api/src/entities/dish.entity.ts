import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { MenuSection } from './menu-section.entity';
import { File } from './file.entity';

@Entity()
export class Dish extends BaseEntity {
  @Column()
  title: string;

  @Column({ type: 'varchar', length: 500 })
  description: string;

  @Column({ type: 'integer' })
  price: number;

  @OneToOne(() => File)
  @JoinColumn()
  file: File;

  @ManyToOne(() => MenuSection, (menuSection) => menuSection.dishes)
  menuSection: MenuSection;
}
