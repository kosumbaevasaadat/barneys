import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Dish } from './dish.entity';
import { Menu } from './menu.entity';

@Entity()
export class MenuSection extends BaseEntity {
  @Column({ unique: true })
  title: string;

  @Column({ nullable: true })
  description: string;

  @OneToMany(() => Dish, (dish) => dish.menuSection)
  dishes: Dish[];

  @ManyToOne(() => Menu, (menu) => menu.sections)
  menu: Menu;
}
