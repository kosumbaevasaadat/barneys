import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToOne,
} from 'typeorm';

import { BaseEntity } from './base.entity';
import { Schedule } from './schedule.entity';
import { File } from './file.entity';
import { ReceiveMethod } from './receive-method.entity';
import { LocationAddress } from './location-address.entity';

@Entity()
export class Branch extends BaseEntity {
  @Column({ unique: true })
  title: string;

  @Column({ type: 'varchar', length: 500 })
  description: string;

  @Column({ unique: true })
  phone: string;

  @Column({ unique: true, nullable: true })
  yelpUrl: string;

  @OneToOne(() => File, { nullable: true })
  @JoinColumn()
  mainImage: File;

  @OneToOne(() => File, { nullable: true })
  @JoinColumn()
  subImage: File;

  @OneToOne(() => LocationAddress)
  @JoinColumn()
  address: LocationAddress;

  @ManyToMany(() => Schedule, (schedule) => schedule.branches)
  @JoinTable({ name: 'branch_schedules' })
  schedules: Schedule[];

  @ManyToMany(() => ReceiveMethod, (receiveMethod) => receiveMethod.branches)
  @JoinTable({ name: 'branch_receive_method' })
  receiveMethods: ReceiveMethod[];
}
