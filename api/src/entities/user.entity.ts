import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { PaymentMethod } from '../shared/enums/payment-method';
import { Roles } from '../shared/enums/user-roles';
import { Address } from './address.entity';
import { Card } from './card.entity';
import { Cart } from './cart.entity';

@Entity()
export class User extends BaseEntity {
  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ unique: true, nullable: true })
  phoneNumber: string;

  @Column({ default: PaymentMethod.CASH })
  paymentMethod: PaymentMethod;

  @Column({ default: Roles.USER })
  role: Roles;

  @OneToOne(() => Address)
  @JoinColumn()
  address: Address;

  @OneToOne(() => Card, (card) => card.user)
  @JoinColumn()
  card: Card;

  @OneToOne(() => Cart, (cart) => cart.user)
  @JoinColumn()
  cart: Cart;
}
