import { Column, Entity, ManyToMany } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Branch } from './branch.entity';

@Entity()
export class Schedule extends BaseEntity {
  @Column()
  days: string;

  @Column()
  hours: string;

  @ManyToMany(() => Branch, (branch) => branch.schedules)
  branches: Branch[];
}
