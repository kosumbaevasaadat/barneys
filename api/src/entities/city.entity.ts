import { Column, Entity } from 'typeorm';

import { BaseEntity } from './base.entity';

@Entity()
export class City extends BaseEntity {
  @Column({ unique: true })
  title: string;
}
