import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { BaseEntity } from './base.entity';
import { File } from './file.entity';
import { Menu } from './menu.entity';
import { Promotion } from './promotion.entity';
import { LocationAddress } from './location-address.entity';

@Entity()
export class Chain extends BaseEntity {
  @Column({ unique: true })
  title: string;

  @Column({ type: 'varchar', length: 500, nullable: true })
  quote: string;

  @Column({ type: 'varchar', length: 2000 })
  story: string;

  @OneToOne(() => File)
  @JoinColumn()
  mainImage: File;

  @ManyToOne(() => Menu)
  menu: Menu;

  @OneToMany(() => Promotion, (promotion) => promotion.chain)
  promotions: Promotion[];

  @ManyToOne(() => LocationAddress)
  address: LocationAddress;
}
