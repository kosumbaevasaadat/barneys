import { Column, Entity } from 'typeorm';

import { BaseEntity } from './base.entity';

@Entity()
export class Address extends BaseEntity {
  @Column()
  street: string;

  @Column()
  apt: number;

  @Column({ nullable: true, default: null })
  building: number;

  @Column({ nullable: true, default: null })
  floor: number;
}
