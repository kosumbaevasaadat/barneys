import { MigrationInterface, QueryRunner } from "typeorm";
import { config } from 'dotenv';

import { User } from '../../src/entities/user.entity';
import { Cart } from '../../src/entities/cart.entity';
import { ProceedStatus } from '../../src/shared/enums/entity-status';

export class AddCartToAdmin1684309128207 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        config();

        const user = await queryRunner.manager.findOne(User,
          { where: { email: process.env.ADMIN_EMAIL } },
        );

        await queryRunner.manager.save(
          queryRunner.manager.create<Cart>(Cart,
            {
                user,
                proceedStatus: ProceedStatus.ACTIVE
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        DELETE FROM cart 
        WHERE user.id IN (
        SELECT id FROM user WHERE email = process.env.ADMIN_EMAIL)`);
    }

}
