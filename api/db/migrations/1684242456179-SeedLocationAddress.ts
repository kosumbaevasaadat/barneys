import { MigrationInterface, QueryRunner } from 'typeorm';

import { LocationAddress } from '../../src/entities/location-address.entity';
import { City } from '../../src/entities/city.entity';
import { State } from '../../src/entities/state.entity';
import { Region } from '../../src/entities/region.entity';

export class SeedLocationAddress1684242456179 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(queryRunner.manager.create<City>(City, { title: 'Santa Monica' }));

    const state = await queryRunner.manager.findOne(State, { where: { title: 'CA' } });

    const regionBayArea = await queryRunner.manager.findOne(Region, { where: { title: 'Bay Area & San Francisco' } });
    const regionLosAngeles = await queryRunner.manager.findOne(Region, { where: { title: 'Los Angeles' } });

    const cityOakland = await queryRunner.manager.findOne(City, { where: { title: 'Oakland' } });
    const cityAlbany = await queryRunner.manager.findOne(City, { where: { title: 'Albany' } });
    const citySanFrancisco = await queryRunner.manager.findOne(City, { where: { title: 'San Francisco' } });
    const cityBerkeley = await queryRunner.manager.findOne(City, { where: { title: 'Berkeley' } });
    const citySantaMonica = await queryRunner.manager.findOne(City, { where: { title: 'Santa Monica' } });
    const cityLosAngeles = await queryRunner.manager.findOne(City, { where: { title: 'Los Angeles' } });
    const cityCalabasas = await queryRunner.manager.findOne(City, { where: { title: 'Calabasas' } });

    const location = queryRunner.manager.create<LocationAddress>(LocationAddress,
      [
        {
          street: 'Shattuck Ave',
          building: 1600,
          unit: 112,
          zipCode: 94709,
          city: cityBerkeley,
          region: regionBayArea,
          state,
        }, {
        street: 'College Ave',
        building: 5819,
        unit: null,
        zipCode: 94618,
        city: cityOakland,
        region: regionBayArea,
        state,
      }, {
        street: 'Solano Avenue',
        building: 1591,
        unit: null,
        zipCode: 94707,
        city: cityAlbany,
        region: regionBayArea,
        state,
      }, {
        street: 'Piedmont Avenue',
        building: 4162,
        unit: null,
        zipCode: 94611,
        city: cityOakland,
        region: regionBayArea,
        state,
      }, {
        street: '24th St.',
        building: 4138,
        unit: null,
        zipCode: 94114,
        city: citySanFrancisco,
        region: regionBayArea,
        state,
      }, {
        street: '26th St.',
        building: 225,
        unit: null,
        zipCode: 90042,
        city: citySantaMonica,
        region: regionLosAngeles,
        state,
      }, {
        street: 'San Vicente Blvd',
        building: 11660,
        unit: null,
        zipCode: 90049,
        city: cityLosAngeles,
        region: regionLosAngeles,
        state,
      }, {
        street: 'Commons Way Ste D',
        building: 4776,
        unit: null,
        zipCode: 91302,
        city: cityCalabasas,
        region: regionLosAngeles,
        state,
      },
      ]
    );

    await queryRunner.manager.save(location);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const buildingsToDelete = [1600, 5819, 1591, 4162, 4138, 225, 11660, 4776];
    await queryRunner.query(
      `DELETE FROM location_address WHERE building IN (${buildingsToDelete.map(building => `'${building}'`).join(', ')})`
    );
    await queryRunner.query(
      `DELETE FROM city WHERE title = 'Santa Monica'`
    );
  }

}
