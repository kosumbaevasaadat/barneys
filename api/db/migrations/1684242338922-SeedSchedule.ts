import { MigrationInterface, QueryRunner } from 'typeorm'
import { Schedule } from '../../src/entities/schedule.entity';

export class SeedSchedule1684242338922 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<Schedule>(Schedule,
        [
          { days: 'Sunday - Thursday', hours: '11AM - 8:30PM' },
          { days: 'Friday & Saturday', hours: '11AM - 9:00PM' },
        ],
      ),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DELETE FROM schedule 
            WHERE (days = 'Sunday - Thursday' AND hours = '11AM - 8:30PM')
            OR (days = 'Friday & Saturday' AND hours = '11AM - 9:00PM')`
    );
  }

}
