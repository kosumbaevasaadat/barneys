import { MigrationInterface, QueryRunner } from 'typeorm'
import { Menu } from '../../src/entities/menu.entity';
import { File } from '../../src/entities/file.entity';

export class SeedMenu1684245791998 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    const image = await queryRunner.manager.save(
      queryRunner.manager.create<File>(File,
        {
          name: '1684246041070.jpeg',
          fileKey: '1684246041070.jpeg',
          contentSize: 513643,
          contentType: 'image/jpeg',
        },
      ));

    await queryRunner.manager.save(
      queryRunner.manager.create<Menu>(Menu, {file: image})
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DELETE FROM menu WHERE id = 1`
    );
    await queryRunner.query(
      `DELETE FROM file WHERE name = '1684246041070.jpeg'`
    );
  }

}
