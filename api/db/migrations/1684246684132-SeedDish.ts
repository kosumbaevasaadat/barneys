import { MigrationInterface, QueryRunner } from 'typeorm';

import { MenuSection } from '../../src/entities/menu-section.entity';
import { File } from '../../src/entities/file.entity';
import { Dish } from '../../src/entities/dish.entity';

export class SeedDish1684246684132 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    const classics = await queryRunner.manager.findOne(MenuSection,
      { where: { title: 'The Classics' } });
    const starters = await queryRunner.manager.findOne(MenuSection,
      { where: { title: 'Starters' } });
    const hamburgers = await queryRunner.manager.findOne(MenuSection,
      { where: { title: 'Specialty gourmet hamburgers' } });

    const [
      turkeyBurger,
      gardenBurger,
      wildAlaskan,
      chicken,
      bigBurger,
      chickenTenders,
      mediterraneanPlate,
      greenSalad,
      smallSalad,
      soup,
      wedgeSalad,
      gastropub,
      baconCheese,
      baja,
      caribbean,
      chipotle,
      french,
      greek,
      guacamole,
      maui,
      western
    ] = await queryRunner.manager.save(
      queryRunner.manager.create<File>(File,
        [
          {
            name: '1684257873363.avif',
            fileKey: '1684257873363.avif',
            contentSize: 3405,
            contentType: 'image/avif',
          }, {
          name: '1684258135271.avif',
          fileKey: '1684258135271.avif',
          contentSize: 3183,
          contentType: 'image/avif',
        }, {
          name: '1684258189216.avif',
          fileKey: '1684258189216.avif',
          contentSize: 3188,
          contentType: 'image/avif',
        }, {
          name: '1680876046004.avif',
          fileKey: '1680876046004.avif',
          contentSize: 3613,
          contentType: 'image/avif',
        }, {
          name: '1680876046004.avif',
          fileKey: '1680876046004.avif',
          contentSize: 3613,
          contentType: 'image/avif',
        }, {
          name: '1680876136078.avif',
          fileKey: '1680876136078.avif',
          contentSize: 2843,
          contentType: 'image/avif',
        }, {
          name: '1680876313382.avif',
          fileKey: '1680876313382.avif',
          contentSize: 3595,
          contentType: 'image/avif',
        }, {
          name: '1680876445933.avif',
          fileKey: '1680876445933.avif',
          contentSize: 4088,
          contentType: 'image/avif',
        }, {
          name: '1680876662612.avif',
          fileKey: '1680876662612.avif',
          contentSize: 4270,
          contentType: 'image/avif',
        }, {
          name: '1680889668415.avif',
          fileKey: '1680889668415.avif',
          contentSize: 4392,
          contentType: 'image/avif',
        }, {
          name: '1680889972960.avif',
          fileKey: '1680889972960.avif',
          contentSize: 2588,
          contentType: 'image/avif',
        }, {
          name: '1680890061862.avif',
          fileKey: '1680890061862.avif',
          contentSize: 4174,
          contentType: 'image/avif',
        }, {
          name: '1680890988114.avif',
          fileKey: '1680890988114.avif',
          contentSize: 2542,
          contentType: 'image/avif',
        }, {
          name: '1680891126659.avif',
          fileKey: '1680891126659.avif',
          contentSize: 3522,
          contentType: 'image/avif',
        }, {
          name: '1680891248755.avif',
          fileKey: '1680891248755.avif',
          contentSize: 3777,
          contentType: 'image/avif',
        }, {
          name: '1680891321135.avif',
          fileKey: '1680891321135.avif',
          contentSize: 3550,
          contentType: 'image/avif',
        }, {
          name: '1680891388226.avif',
          fileKey: '1680891388226.avif',
          contentSize: 3582,
          contentType: 'image/avif',
        }, {
          name: '1680891472902.avif',
          fileKey: '1680891472902.avif',
          contentSize: 2711,
          contentType: 'image/avif',
        }, {
          name: '1680891527983.avif',
          fileKey: '1680891527983.avif',
          contentSize: 3058,
          contentType: 'image/avif',
        }, {
          name: '1680891607426.avif',
          fileKey: '1680891607426.avif',
          contentSize: 3679,
          contentType: 'image/avif',
        }, {
          name: '1680891695967.avif',
          fileKey: '1680891695967.avif',
          contentSize: 2964,
          contentType: 'image/avif',
        }, {
          name: '1680891763137.avif',
          fileKey: '1680891763137.avif',
          contentSize: 3091,
          contentType: 'image/avif',
        },
        ]));

    await queryRunner.manager.save(
      queryRunner.manager.create<Dish>(Dish,
        [
          {
            title: 'Barney\'s Turkey Burger',
            description: 'all natural turkey patty',
            price: 1645,
            file: turkeyBurger,
            menuSection: classics,
          }, {
          title: 'Garden Burger',
          description: 'served on a whole wheat bun',
          price: 1425,
          file: gardenBurger,
          menuSection: classics,
        }, {
          title: 'Wild Alaskan Salmon Patty',
          description: 'served on a whole wheat bun',
          price: 1645,
          file: wildAlaskan,
          menuSection: classics,
        }, {
          title: 'Barney\'s Chicken',
          description: 'flamed grilled chicken breast',
          price: 1755,
          file: chicken,
          menuSection: classics,
        }, {
          title: 'Big Barney\'s Burger',
          description: '1 lb custom blend, flame broiled 100% natural chuck, served on a french roll',
          price: 2295,
          file: bigBurger,
          menuSection: classics,
        }, {
          title: 'Chicken Tenders',
          description: 'served with BBQ sauce',
          price: 995,
          file: chickenTenders,
          menuSection: starters,
        }, {
          title: 'Mediterranean Plate',
          description: 'housemade falafel hummus, feta, cucumber, tomato, onion, kalamata olives, pepperoncini, grilled lemon & pita bread',
          price: 1485,
          file: mediterraneanPlate,
          menuSection: starters,
        }, {
          title: 'Mixed Green Salad',
          description: 'baby green mix, tomato, cucumber & vinaigrette',
          price: 825,
          file: greenSalad,
          menuSection: starters,
        }, {
          title: 'Small Barney\'s Salad',
          description: 'spring mix, romaine, black beans, garbanzo, kalamata olives, tomato, sprouts, peperoncini, & croutons, served with your choice of dressing',
          price: 825,
          file: smallSalad,
          menuSection: starters,
        }, {
          title: 'Soup of the Day',
          description: 'a hearty bowl of our housemade soup of the day',
          price: 850,
          file: soup,
          menuSection: starters,
        }, {
          title: 'Wedge Salad',
          description: 'iceberg wedge, tomato, bacon, blue cheese, & blue cheese dressing',
          price: 965,
          file: wedgeSalad,
          menuSection: starters,
        }, {
          title: 'Gastropub',
          description: 'provolone cheese, bacon, fried egg & housemade blackened chips, on a pretzel bun',
          price: 1945,
          file: gastropub,
          menuSection: hamburgers,
        }, {
          title: 'Bacon Cheese',
          description: 'bacon, cheddar cheese, lettuce, tomato, red onion, pickles, & 1000 Island dressing',
          price: 1925,
          file: baconCheese,
          menuSection: hamburgers,
        }, {
          title: 'Baja',
          description: 'bacon, avocado, jack cheese & pico de gallo on the side',
          price: 1945,
          file: baja,
          menuSection: hamburgers,
        }, {
          title: 'Caribbean',
          description: 'sauteed zucchini, bell peppers, carrots, spicy jerk sauce',
          price: 1843,
          file: caribbean,
          menuSection: hamburgers,
        }, {
          title: 'Chipotle',
          description: 'spicy chiptole sauce, jack cheese & sauteed mushrooms',
          price: 1945,
          file: chipotle,
          menuSection: hamburgers,
        }, {
          title: 'French',
          description: 'blue cheese & bacon, on a toast baugette',
          price: 1945,
          file: french,
          menuSection: hamburgers,
        }, {
          title: 'Greek',
          description: 'feta, avocado, lettuce, cucumbers, tomato, in a whole wheat pita',
          price: 1945,
          file: greek,
          menuSection: hamburgers,
        }, {
          title: 'Guacamole',
          description: 'jack cheese, guacamole, sour cream',
          price: 1925,
          file: guacamole,
          menuSection: hamburgers,
        }, {
          title: 'Maui Waui',
          description: 'teriyaki glaze & grilled pineapple',
          price: 1875,
          file: maui,
          menuSection: hamburgers,
        }, {
          title: 'Western',
          description: 'cheddar cheese, bbq sauce, bacon, sauteed onions',
          price: 1945,
          file: western,
          menuSection: hamburgers,
        },
        ])
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const titlesToDelete = [
      'Barney\'s Turkey Burger',
      'Garden Burger',
      'Wild Alaskan Salmon Patty',
      'Barney\'s Chicken',
      'Big Barney\'s Burger',
      'Chicken Tenders',
      'Mediterranean Plate',
      'Mixed Green Salad',
      'Small Barney\'s Salad',
      'Soup of the Day',
      'Wedge Salad',
      'Gastropub',
      'Bacon Cheese',
      'Baja',
      'Caribbean',
      'Chipotle',
      'French',
      'Greek',
      'Guacamole',
      'Maui Waui',
      'Western',
    ];
    await queryRunner.query(
      `DELETE FROM dish WHERE title IN (${titlesToDelete.map(title => `'${title}'`).join(', ')})`
    )

    const filesToDelete = [
      '1684257873363.avif',
      '1684258135271.avif',
      '1684258189216.avif',
      '1680876046004.avif',
      '1680876046004.avif',
      '1680876136078.avif',
      '1680876313382.avif',
      '1680876445933.avif',
      '1680876662612.avif',
      '1680889668415.avif',
      '1680889972960.avif',
      '1680890061862.avif',
      '1680890988114.avif',
      '1680891126659.avif',
      '1680891248755.avif',
      '1680891321135.avif',
      '1680891388226.avif',
      '1680891472902.avif',
      '1680891527983.avif',
      '1680891607426.avif',
      '1680891695967.avif',
      '1680891763137.avif',
    ];
    await queryRunner.query(
      `DELETE FROM file WHERE name IN (${filesToDelete.map(name => `'${name}'`).join(', ')})`
    )
  }

}
