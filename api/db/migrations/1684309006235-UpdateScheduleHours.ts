import { MigrationInterface, QueryRunner } from "typeorm"

export class UpdateScheduleHours1684309006235 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.manager.query(
          `UPDATE schedule SET hours = '11:00 AM-8:30 PM' WHERE id = 1;`
        );
        await queryRunner.manager.query(
          `UPDATE schedule SET hours = '11:00 AM-9:00 PM' WHERE id = 2;`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.manager.query(
          `UPDATE schedule SET hours = '11AM - 8:30PM' WHERE id = 1;`
        );
        await queryRunner.manager.query(
          `UPDATE schedule SET hours = '11AM - 9:00PM' WHERE id = 2;`
        );
    }

}
