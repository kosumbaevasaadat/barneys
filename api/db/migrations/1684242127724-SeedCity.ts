import { MigrationInterface, QueryRunner } from 'typeorm';
import { City } from '../../src/entities/city.entity';

export class SeedCity1684242127724 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<City>(City,
        [
          { title: 'Berkeley' },
          { title: 'Oakland' },
          { title: 'Albany' },
          { title: 'San Francisco' },
          { title: 'Los Angeles' },
          { title: 'Calabasas' },
        ]),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const titlesToDelete = ['Berkeley', 'Oakland', 'Albany', 'San Francisco', 'Los Angeles', 'Calabasas'];
    await queryRunner.query(
      `DELETE FROM city WHERE title IN (${titlesToDelete.map(title => `'${title}'`).join(', ')})`
    )
  }

}
