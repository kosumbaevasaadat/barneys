import { MigrationInterface, QueryRunner } from "typeorm"

import { Menu } from '../../src/entities/menu.entity';
import { MenuSection } from '../../src/entities/menu-section.entity';

export class SeedMenuSection1684246490497 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const menu = await queryRunner.manager.findOne(Menu,
          { where: { id: 1 } },
        );

        await queryRunner.manager.save(
          queryRunner.manager.create<MenuSection>(MenuSection,
            [
                {
                    title: 'The Classics',
                    description: 'All our classics come with lettuce, tomato, red onion, pickle & 1000 island. Substitute for a gluten-free or pretzel bun for an additional charge.',
                    menu,
                }, {
                title: 'Starters',
                description: null,
                menu,
            }, {
                title: 'Specialty gourmet hamburgers',
                description: 'For more protein options, choose from the following: turkey, garden patty, chicken, salmon patty or beyond meat (additional charge).',
                menu,
            },
            ]),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const titlesToDelete = ['The Classics', 'Starters', 'Specialty gourmet hamburgers'];
        await queryRunner.query(
          `DELETE FROM menuSection WHERE title IN (${titlesToDelete.map(title => `'${title}'`).join(', ')})`
        );
    }

}
