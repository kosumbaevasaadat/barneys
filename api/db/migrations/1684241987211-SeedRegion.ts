import { MigrationInterface, QueryRunner } from 'typeorm';
import { Region } from '../../src/entities/region.entity';

export class SeedRegion1684241987211 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<Region>(Region,
        [
          { title: 'Bay Area & San Francisco' },
          { title: 'Los Angeles' },
        ])
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const titlesToDelete = ['Bay Area & San Francisco', 'Los Angeles'];
    await queryRunner.query(
      `DELETE FROM region WHERE title IN (${titlesToDelete.map(title => `'${title}'`).join(', ')})`
    );
  }

}
