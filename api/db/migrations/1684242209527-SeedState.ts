import { MigrationInterface, QueryRunner } from 'typeorm'
import { State } from '../../src/entities/state.entity';

export class SeedState1684242209527 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<State>(State,
        [
          { title: 'CA' },
        ])
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DELETE FROM state WHERE title = 'CA'`
    );
  }

}
