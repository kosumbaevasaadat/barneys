import { MigrationInterface, QueryRunner } from 'typeorm';

import { File } from '../../src/entities/file.entity';
import { LocationAddress } from '../../src/entities/location-address.entity';
import { Branch } from '../../src/entities/branch.entity';
import { Schedule } from '../../src/entities/schedule.entity';
import { ReceiveMethod } from '../../src/entities/receive-method.entity';

export class SeedBranches1684243746836 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    const [
      mainImage1, subImage1,
      mainImage2, subImage2,
      mainImage3, subImage3,
      mainImage4, subImage4,
      mainImage5, subImage5,
      mainImage6, subImage6,
      mainImage7, subImage7,
      mainImage8, subImage8,
    ] = await queryRunner.manager.save(
      queryRunner.manager.create<File>(File, [
        {
          name: '1680852439152.jpeg',
          fileKey: '1680852439152.jpeg',
          contentSize: 829426,
          contentType: 'image/jpeg',
        }, {
          name: '1680852458706.jpeg',
          fileKey: '1680852458706.jpeg',
          contentSize: 70832,
          contentType: 'image/jpeg',
        },

        {
          name: '1680852472328.jpeg',
          fileKey: '1680852472328.jpeg',
          contentSize: 707706,
          contentType: 'image/jpeg',
        }, {
          name: '1680852482440.jpeg',
          fileKey: '1680852482440.jpeg',
          contentSize: 102104,
          contentType: 'image/jpeg',
        },

        {
          name: '1680852492849.jpeg',
          fileKey: '1680852492849.jpeg',
          contentSize: 897164,
          contentType: 'image/jpeg',
        }, {
          name: '1680852504890.jpeg',
          fileKey: '1680852504890.jpeg',
          contentSize: 62870,
          contentType: 'image/jpeg',
        },

        {
          name: '1680852514132.jpeg',
          fileKey: '1680852514132.jpeg',
          contentSize: 851490,
          contentType: 'image/jpeg',
        }, {
          name: '1680852525434.jpeg',
          fileKey: '1680852525434.jpeg',
          contentSize: 99257,
          contentType: 'image/jpeg',
        },

        {
          name: '1680852533951.jpeg',
          fileKey: '1680852533951.jpeg',
          contentSize: 836498,
          contentType: 'image/jpeg',
        }, {
          name: '1680852547362.jpeg',
          fileKey: '1680852547362.jpeg',
          contentSize: 88463,
          contentType: 'image/jpeg',
        },

        {
          name: '1680852555572.jpeg',
          fileKey: '1680852555572.jpeg',
          contentSize: 910678,
          contentType: 'image/jpeg',
        }, {
          name: '1680852569219.jpeg',
          fileKey: '1680852569219.jpeg',
          contentSize: 84319,
          contentType: 'image/jpeg',
        },

        {
          name: '1680852579227.jpeg',
          fileKey: '1680852579227.jpeg',
          contentSize: 695382,
          contentType: 'image/jpeg',
        }, {
          name: '1680852588925.jpeg',
          fileKey: '1680852588925.jpeg',
          contentSize: 118918,
          contentType: 'image/jpeg',
        },

        {
          name: '1680852598275.jpeg',
          fileKey: '1680852598275.jpeg',
          contentSize: 99099,
          contentType: 'image/jpeg',
        }, {
          name: '1680852606688.jpeg',
          fileKey: '1680852606688.jpeg',
          contentSize: 127329,
          contentType: 'image/jpeg',
        },
      ]),
    );

    const address = await queryRunner.manager.find(LocationAddress);
    const schedules = await queryRunner.manager.find(Schedule);
    const receiveMethods = await queryRunner.manager.find(ReceiveMethod);

    await queryRunner.manager.save(
      queryRunner.manager.create<Branch>(Branch,
        [
          {
            title: 'Berkeley - Shattuck',
            description: 'Located within the famed Gourmet Ghetto, our Barney’s Gourmet Hamburger location on Shattuck is a cozy place to socialize with classmates, before or after class. Enjoy our fresh Signature Hamburgers, made with our secret blend of Angus Chuck, 100% all-natural beef. It’s the way we’ve been doing it for over 40 years! ',
            phone: '+1 (510) 849-2827',
            yelpUrl: 'https://www.yelp.com/biz/barneys-gourmet-hamburgers-berkeley-2',
            mainImage: mainImage1,
            subImage: subImage1,
            address: address[0],
            schedules: schedules,
            receiveMethods: receiveMethods,
          }, {
          title: 'College Ave',
          description: 'Located in trendy Easy Bay, our location on College Ave. boasts an outdoor patio, perfect on a warm summer day. Large windows provide this location with ample natural light, the ideal setting for the perfect instagram shot! Come join us for a Gourmet Hamburger under the stars… and don’t forget about our Milkshakes too!',
          phone: '+1 (510) 601-0444',
          yelpUrl: null,
          mainImage: mainImage2,
          subImage: subImage2,
          address: address[1],
          schedules,
          receiveMethods,
        }, {
          title: 'Berkeley - Solano',
          description: 'Located a stone’s throw away from UC Berkeley, Barney’s Gourmet Hamburgers on Solano Ave. is known for their friendly, warm, service and charming atmosphere. It’s the perfect spot to reconnect with family & friends. Come join us and don’t forget to try our famous Curly Fries & housemade Ranch… it’s been a Barney’s staple for over 40 years.',
          phone: '+1 (510) 526-8185',
          yelpUrl: 'https://www.yelp.com/biz/barneys-gourmet-hamburgers-berkeley?start=440',
          mainImage: mainImage3,
          subImage: subImage3,
          address: address[2],
          schedules,
          receiveMethods,
        }, {
          title: 'Piedmont',
          description: 'Nestled in the heart of Oakland, our location off bustling Piedmont Ave. offers a stylish dining room with dark wooden accents, open kitchen, and the best burgers in town! It’s the perfect place for a quick lunch with colleagues, or a weekend escape with family & friends… either way, you’ll be in burger nirvana!',
          phone: '+1 (510) 655-7180',
          yelpUrl: null,
          mainImage: mainImage4,
          subImage: subImage4,
          address: address[3],
          schedules,
          receiveMethods,
        }, {
          title: 'San Francisco',
          description: 'In the heart of San Francisco, Barney’s Gourmet Hamburgers has been winning hearts for over 40 years, since 1978 to be exact. We are the originators of the Gourmet Hamburger, and we’ve won hundreds of awards to prove it! Stop on by our charming location in SF for one of our signature burgers, like our Baja Burger, topped with creamy Avocado and Pico de Gallo, it’s absolutely delicious! Don’t forget the fries… you’d be mad not to! #BarneysHamburgers',
          phone: '+1 (415) 282-7770',
          yelpUrl: 'https://www.yelp.com/biz/barneys-gourmet-hamburgers-san-francisco-2',
          mainImage: mainImage5,
          subImage: subImage5,
          address: address[4],
          schedules,
          receiveMethods,
        }, {
          title: 'Santa Monica',
          description: 'Located inside the iconic Brentwood Country Mart, Barney’s Gourmet Hamburgers is the perfect place to go before or after a day of shopping. Sit by the beautiful fire pit in the outdoor space, while enjoying one of our famous hamburgers alongside a thick Milkshake & crispy Curly Fries. Barney’s Gourmet Hamburgers is also the perfect place for a power lunch with colleagues.',
          phone: '+1 (310) 899-0133',
          yelpUrl: 'https://www.yelp.com/biz/barneys-gourmet-hamburgers-santa-monica',
          mainImage: mainImage6,
          subImage: subImage6,
          address: address[5],
          schedules,
          receiveMethods,
        }, {
          title: 'Brentwood',
          description: 'Located along bustling San Vicente Blvd. in Santa Monica, Barney’s Gourmet Hamburgers is just a few miles from the world famous Santa Monica Pier. The perfect place for a quaint lunch, our San Vicente location is an intimate space with the best burgers in town! Whether you are taking your food down to the beach, or stopping by for a quick power lunch with co-workers, Barney’s San Vicente will not disappoint.',
          phone: '+1 (310) 447-6000',
          yelpUrl: 'https://www.yelp.com/biz/barneys-gourmet-hamburgers-los-angeles-4',
          mainImage: mainImage7,
          subImage: subImage7,
          address: address[6],
          schedules,
          receiveMethods,
        }, {
          title: 'Calabasas',
          description: 'Located inside the luxurious Calabasas Commons, Barney’s Gourmet Hamburgers is the perfect place to go before or after a day of shopping. Sit in our gorgeous outdoor space, while enjoying one of our famous hamburgers alongside a thick Milkshake, draft beer, wine & crispy Curly Fries. Barney’s Gourmet Hamburgers is also the perfect place for a power lunch with colleagues or a place to take the kids on the weekends for a fun night out.',
          phone: '+1 (818) 854-6026',
          yelpUrl: null,
          mainImage: mainImage8,
          subImage: subImage8,
          address: address[7],
          schedules,
          receiveMethods,
        },
        ]
      ),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const titlesToDelete = ['Berkeley - Shattuck', 'College Ave', 'Berkeley - Solano', 'Piedmont', 'San Francisco', 'Santa Monica', 'Brentwood', 'Calabasas'];
    await queryRunner.query(
      `DELETE FROM branch WHERE title IN (${titlesToDelete.map(title => `'${title}'`).join(', ')})`
    );
  }

}
