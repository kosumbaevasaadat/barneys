import { MigrationInterface, QueryRunner } from 'typeorm'
import { ReceiveMethod } from '../../src/entities/receive-method.entity';

export class SeedReceiveMethods1684241890787 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<ReceiveMethod>(ReceiveMethod,
        [
          { title: 'dine-in' },
          { title: 'take-out' },
          { title: 'delivery' },
        ],
      ),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const titlesToDelete = ['dine-in', 'take-out', 'delivery'];
    await queryRunner.query(`DELETE FROM receiveMethod WHERE title IN (${titlesToDelete.map(title => `'${title}'`).join(', ')})`);
  }

}
