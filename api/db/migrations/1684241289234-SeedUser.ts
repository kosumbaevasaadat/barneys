import { MigrationInterface, QueryRunner } from 'typeorm'
import { config } from 'dotenv';
import * as bcrypt from 'bcrypt';

import { User } from '../../src/entities/user.entity';
import { Roles } from '../../src/shared/enums/user-roles';


export class SeedUser1684241289234 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    config();

    await queryRunner.query(`DELETE FROM user WHERE email='${process.env.ADMIN_EMAIL}'`);

    const encodedPassword = (await bcrypt.hash(process.env.ADMIN_PASSWORD, 10)) as string;

    await queryRunner.manager.save(
      queryRunner.manager.create<User>(User, {
        firstName: process.env.ADMIN_FIRST_NAME,
        lastName: process.env.ADMIN_LAST_NAME,
        email: process.env.ADMIN_EMAIL,
        password: encodedPassword,
        role: Roles.ADMIN,
        phoneNumber: null
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    config();

    await queryRunner.query(`DELETE FROM user WHERE email = ${process.env.ADMIN_EMAIL}`);
  }

}
