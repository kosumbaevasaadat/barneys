import { MigrationInterface, QueryRunner } from 'typeorm';

import { File } from '../../src/entities/file.entity';
import { Menu } from '../../src/entities/menu.entity';
import { Promotion } from '../../src/entities/promotion.entity';
import { Chain } from '../../src/entities/chain.entity';
import { State } from '../../src/entities/state.entity';
import { Region } from '../../src/entities/region.entity';
import { City } from '../../src/entities/city.entity';
import { LocationAddress } from '../../src/entities/location-address.entity';

export class SeedChain1684260245749 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    const mainImage = await queryRunner.manager.save(
      queryRunner.manager.create<File>(File, {
        name: '1680284099771.png',
        fileKey: '1680284099771.png',
        contentSize: 4011745,
        contentType: 'image/png',
      }));
    const menu = await queryRunner.manager.findOne(Menu,
      { where: { id: 1 } });
    const promotions = await queryRunner.manager.find(Promotion);

    const state = await queryRunner.manager.findOne(State, { where: { title: 'CA' } });
    const regionLosAngeles = await queryRunner.manager.findOne(Region, { where: { title: 'Los Angeles' } });
    const cityLosAngeles = await queryRunner.manager.findOne(City, { where: { title: 'Los Angeles' } });

    const address = await queryRunner.manager.save(
      queryRunner.manager.create<LocationAddress>(LocationAddress,
        {
          street: 'San Vicente Blvd',
          building: 11660,
          unit: null,
          zipCode: 90049,
          city: cityLosAngeles,
          region: regionLosAngeles,
          state,
        })
    );

    await queryRunner.manager.save(
      queryRunner.manager.create<Chain>(Chain,
        {
          title: 'Barney\'s Gourmet Hamburgers',
          quote: 'You deserve to be cherished. Through care, competence, attentiveness & excellence we act on this belief when you’re in our restaurant. Each welcoming visit, each interaction we share with you, each creative, delightful bite of our farm-fresh-to table food you savor, is our way of appreciating you.',
          story: 'In 1978, we opened our first burger shop in Oakland and introduced our award-winning concept of gourmet hamburgers — yes, we were one of the first! Since then we’ve opened nine other burger restaurants and expanded our menu so that everyone who walks in can find something they’ll love. You can also enjoy burger delivery and pick up so that you can have Barney’s anywhere you go.\n' +
            '\n' +
            'We are grateful to be winning awards still, but we are most proud of becoming a tradition for the families and friends in our communities.',
          mainImage,
          menu,
          promotions,
          address,
        }
      )
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.query(
      `DELETE FROM file WHERE name = '1680284099771.png'`
    );
    await queryRunner.manager.query(
      `DELETE FROM locationAddress WHERE building = 11660`
    );
    await queryRunner.manager.query(
      `DELETE FROM chain WHERE title = 'Barney\\'s Gourmet Hamburgers'`
    );
  }

}
