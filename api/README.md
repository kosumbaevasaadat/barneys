# Project REST API

## Description

This application is a REST API for the fast food restaurant chain "Barney's Gourmet Hamburgers". This project built with Nest.js, TypeORM, and MySQL. It provides endpoints for creating, reading, updating, and deleting resources.

## Getting Started

1. Clone the repository:

```bash
  git clone https://git.syberry.com/s.kosumbaeva/aqua-playground.git
```

2. Install the dependencies:

```bash
  cd aqua-playground/api
  npm install
```

3. Create a .env file in the root of the project with the following contents:

```
# App
PORT=

# DB
DB_TYPE=
DB_URL=
DB_PORT=
DB_USER=
DB_PASS=
DB_NAME=
DB_SYNCHRONIZE=
DB_ENTITIES=

#JWT
JWT_SECRET=
JWT_ACCESS_EXP=
JWT_REFRESH_EXP=

#Admin
ADMIN_EMAIL=
ADMIN_PASSWORD=

#YandexS3
BUCKET_NAME =
ACCESS_KEY_ID =
SECRET_ACCESS_KEY =
```

## Database

Stack: MySQL

1. Install Docker on your machine if you haven't already.

2. Pull the official MySQL Docker image from Docker Hub using the following command in your terminal or command prompt:

```bash
  docker pull mysql
```

3. Start a new Docker container from the MySQL image using the following command:

```bash
  docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=your_password -p 3306:3306 -d mysql
```

Replace your_password with your chosen password.

This command creates a new Docker container named my-mysql and sets the MySQL root password to your chosen password. It also maps port 3306 in the container to port 3306 on your local machine, so you can access the MySQL server using localhost:3306.

4. Verify that the container is running by running the following command:

```bash
  docker ps
```

This should show you a list of all the running Docker containers on your machine, including the one you just created.`

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
