# Aqua Playground Frontend

## Description

This is a frontend application for the fast food restaurant chain "Barney's Gourmet Hamburgers". The application is based on Angular, Angular Material technologies, and also uses NgRx and RxJS.

## Getting started

To get started with this application, follow these steps:

1. Clone the repository:

```bash
  git clone https://git.syberry.com/s.kosumbaeva/aqua-playground.git
```

2. Install the dependencies:

```bash
  cd aqua-playground/front
  npm install
```

3. Run the development server:

```bash
  ng serve
```

4. Open your browser and navigate to [http://localhost:4200](http://localhost:4200) to view the app.
