import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { LocationDetailsComponent } from './pages/location-details/location-details.component';
import { MenuComponent } from './pages/menu/menu.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { MapComponent } from './pages/map/map.component';
import { BranchComponent } from './pages/branch/branch.component';
import { CartComponent } from './pages/cart/cart.component';
import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';
import { NotFoundComponent } from './not-found.component';
import { EditDishComponent } from './pages/edit-dish/edit-dish.component';
import { RoleGuardService } from './shared/services/role-guard.service';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'locations', component: LocationsComponent },
  { path: 'locations/:id', component: LocationDetailsComponent },
  { path: 'menu', component: MenuComponent },
  { path: 'our-story', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'map', component: MapComponent },
  { path: 'branch/:id', component: BranchComponent },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [RoleGuardService],
    data: { roles: ['ADMIN', 'USER'] },
  },
  {
    path: 'profiles/:id/edit',
    component: EditProfileComponent,
    canActivate: [RoleGuardService],
    data: { roles: ['ADMIN', 'USER'] },
  },
  { path: 'dishes/:id/edit', component: EditDishComponent },
  { path: 'dishes/new', component: EditDishComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
