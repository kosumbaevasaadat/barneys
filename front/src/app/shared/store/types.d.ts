import { ErrorInterface, User } from '../models/user.model';
import { Menu } from '../models/menu.model';
import { Branch } from '../models/branch.model';
import { Cart } from '../models/cart.model';

export type UsersState = {
  token: null | string,
  user: null | User,
  updateLoading: boolean,
  updateError: null | ErrorInterface,
  updateEmailLoading: boolean,
  updateEmailError: null | ErrorInterface,
  updatePasswordLoading: boolean,
  updatePasswordError: null | ErrorInterface,
  registerLoading: boolean,
  registerError: null | ErrorInterface,
  loginLoading: boolean,
  loginError: null | ErrorInterface,
};

export type MenuState = {
  item: null | Menu,
  fetchLoading: boolean,
}

export type BranchesState = {
  item: null | Branch,
  items: null | Branch[],
  fetchItemLoading: boolean,
  fetchItemsLoading: boolean,
}

export type CartsState = {
  item: null | Cart,
  fetchLoading: boolean,
}

export type CartProductState = {
  createLoading: boolean,
  updateLoading: boolean,
  removeLoading: boolean,
}

export type AppState = {
  users: UsersState,
  menu: MenuState,
  branches: BranchesState,
  carts: CartsState,
  cartProduct: CartProductState,
};
