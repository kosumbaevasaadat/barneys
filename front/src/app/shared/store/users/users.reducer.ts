import { UsersState } from '../types';
import { createReducer, on } from '@ngrx/store';
import {
  getProfileSuccess,
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  logoutUser,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess,
  updateEmailFailure,
  updateEmailRequest,
  updateEmailSuccess,
  updatePasswordFailure,
  updatePasswordRequest,
  updatePasswordSuccess,
  updateUserFailure,
  updateUserRequest,
  updateUserSuccess
} from './users.actions';

const initialState: UsersState = {
  token: null,
  user: null,
  updateLoading: false,
  updateError: null,
  updateEmailLoading: false,
  updateEmailError: null,
  updatePasswordLoading: false,
  updatePasswordError: null,
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null,
};

export const usersReducer = createReducer(
  initialState,
  on(registerUserRequest, state => ({ ...state, registerLoading: true, registerError: null })),
  on(registerUserSuccess, (state, { userTokenData }) => ({
    ...state,
    registerLoading: false,
    user: userTokenData.user,
    token: userTokenData.token,
  })),
  on(registerUserFailure, (state, { error }) => ({ ...state, registerLoading: false, registerError: error })),
  on(loginUserRequest, state => ({ ...state, loginLoading: true, loginError: null })),
  on(loginUserSuccess, (state, { userTokenData }) => ({
    ...state,
    loginLoading: false,
    user: userTokenData.user,
    token: userTokenData.token,
  })),
  on(loginUserFailure, (state, { error }) => ({ ...state, loginLoading: false, loginError: error })),
  on(logoutUser, state => ({ ...state, user: null, token: null })),
  on(updateUserRequest, state => ({ ...state, updateLoading: true, updateError: null })),
  on(updateUserSuccess, (state, { user }) => ({ ...state, updateLoading: false, user })),
  on(updateUserFailure, (state, { error }) => ({ ...state, updateLoading: false, updateError: error })),
  on(updateEmailRequest, state => ({ ...state, updateEmailLoading: true, updateEmailError: null })),
  on(updateEmailSuccess, state => ({ ...state, updateEmailLoading: false })),
  on(updateEmailFailure, (state, { error }) => ({ ...state, updateEmailLoading: false, updateEmailError: error })),
  on(updatePasswordRequest, state => ({ ...state, updatePasswordLoading: true, updatePasswordError: null })),
  on(updatePasswordSuccess, state => ({ ...state, updatePasswordLoading: false })),
  on(updatePasswordFailure, (state, { error }) => ({
    ...state,
    updatePasswordLoading: false,
    updatePasswordError: error
  })),
  on(getProfileSuccess, (state, { user }) => ({ ...state, user })),
);
