import { map, mergeMap, tap } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';

import { HelpersService } from '../../services/helpers.service';
import { UsersService } from '../../services/users.service';
import {
  getProfileFailure,
  getProfileRequest,
  getProfileSuccess,
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  logoutUser,
  logoutUserRequest,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess,
  updateEmailFailure,
  updateEmailRequest,
  updateEmailSuccess, updatePasswordFailure, updatePasswordRequest, updatePasswordSuccess,
  updateUserFailure,
  updateUserRequest,
  updateUserSuccess
} from './users.actions';
import { MESSAGES } from '../../constants';
import { Store } from '@ngrx/store';
import { AppState } from '../types';

@Injectable()
export class UsersEffects {

  constructor(private store: Store<AppState>,
              private actions: Actions,
              private router: Router,
              private usersService: UsersService,
              private helpers: HelpersService,) {
  }

  registerUser = createEffect(() => this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({ userData }) => this.usersService.register(userData).pipe(
      map(userTokenData => registerUserSuccess({ userTokenData })),
      tap(() => {
        this.helpers.openSnackBar(MESSAGES.registerSuccess);
      }),
      this.helpers.catchServerError(registerUserFailure),
    )),
  ));

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({ userData }) => this.usersService.login(userData).pipe(
      map(userTokenData => loginUserSuccess({ userTokenData })),
      tap(() => {
        this.helpers.openSnackBar(MESSAGES.loginSuccess);
      }),
      this.helpers.catchServerError(loginUserFailure),
    )),
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(() => {
      return this.usersService.logout().pipe(
        map(() => logoutUser()),
        tap(() => {
          this.helpers.openSnackBar(MESSAGES.logoutSuccess);
        }),
      );
    }),
  ));

  getProfile = createEffect(() => this.actions.pipe(
    ofType(getProfileRequest),
    mergeMap(() => this.usersService.getProfile().pipe(
      map((user) => getProfileSuccess({ user })),
      this.helpers.catchServerError(getProfileFailure),
    )),
  ));

  updateUser = createEffect(() => this.actions.pipe(
    ofType(updateUserRequest),
    mergeMap(({ userData }) => this.usersService.update(userData).pipe(
      map((user) => updateUserSuccess({ user })),
      tap(() => {
        this.helpers.openSnackBar(MESSAGES.updated);
      }),
      this.helpers.catchServerError(updateUserFailure),
    )),
  ));

  updateEmail = createEffect(() => this.actions.pipe(
    ofType(updateEmailRequest),
    mergeMap(({ data }) => this.usersService.updateEmail(data).pipe(
      map(() => updateEmailSuccess()),
      tap(() => {
        this.helpers.openSnackBar(MESSAGES.updated);
        this.store.dispatch(getProfileRequest());
      }),
      this.helpers.catchServerError(updateEmailFailure),
    )),
  ));

  updatePassword = createEffect(() => this.actions.pipe(
    ofType(updatePasswordRequest),
    mergeMap(({ data }) => this.usersService.updatePassword(data).pipe(
      map(() => updatePasswordSuccess()),
      tap(() => {
        this.helpers.openSnackBar(MESSAGES.updated);
      }),
      this.helpers.catchServerError(updatePasswordFailure),
    )),
  ));
}
