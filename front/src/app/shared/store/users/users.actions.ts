import { createAction, props } from '@ngrx/store';
import {
  ErrorInterface,
  LoginUserData,
  RegisterUserData,
  UpdateEmailData,
  UpdatePasswordData,
  UpdateUserData,
  User,
  UserTokenData
} from '../../models/user.model';

export const registerUserRequest = createAction('[Users] Register Request', props<{ userData: RegisterUserData }>());
export const registerUserSuccess = createAction('[Users] Register Success', props<{ userTokenData: UserTokenData }>());
export const registerUserFailure = createAction('[Users] Register Failure', props<{ error: null | ErrorInterface }>());

export const loginUserRequest = createAction('[Users] Login Request', props<{ userData: LoginUserData }>());
export const loginUserSuccess = createAction('[Users] Login Success', props<{ userTokenData: UserTokenData }>());
export const loginUserFailure = createAction('[Users] Login Failure', props<{ error: null | ErrorInterface }>());

export const getProfileRequest = createAction('[Users] Get Profile Request');
export const getProfileSuccess = createAction('[Users] Get Profile Success', props<{ user: User }>());
export const getProfileFailure = createAction('[Users] Get Profile Failure');

export const updateUserRequest = createAction('[Users] Update Request', props<{ userData: UpdateUserData }>());
export const updateUserSuccess = createAction('[Users] Update Success', props<{ user: User }>());
export const updateUserFailure = createAction('[Users] Update Failure', props<{ error: null | ErrorInterface }>());

export const updateEmailRequest = createAction('[Users] Update Email Request', props<{ data: UpdateEmailData }>());
export const updateEmailSuccess = createAction('[Users] Update Email Success');
export const updateEmailFailure = createAction('[Users] Update Email Failure', props<{ error: null | ErrorInterface }>());

export const updatePasswordRequest = createAction('[Users] Update Password Request', props<{ data: UpdatePasswordData }>());
export const updatePasswordSuccess = createAction('[Users] Update Password Success');
export const updatePasswordFailure = createAction('[Users] Update Password Failure', props<{ error: null | ErrorInterface }>());

export const logoutUser = createAction('[Users] Logout User');
export const logoutUserRequest = createAction('[Users] Server Logout Request');
