import { catchError, map, mergeMap, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { AppState } from '../types';
import { HelpersService } from '../../services/helpers.service';
import { BranchesService } from '../../services/branches.service';
import {
  fetchBranchesFailure,
  fetchBranchesRequest,
  fetchBranchesSuccess,
  fetchBranchFailure,
  fetchBranchRequest,
  fetchBranchSuccess
} from './branches.actions';
import { ERRORS } from '../../constants';

@Injectable()
export class BranchesEffects {
  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    private helpers: HelpersService,
    private branchesService: BranchesService,
  ) {
  }

  fetchBranches = createEffect(() => this.actions.pipe(
    ofType(fetchBranchesRequest),
    mergeMap(() => this.branchesService.fetchAll().pipe(
      map(items => fetchBranchesSuccess({ items })),
      catchError(() => {
        this.helpers.openSnackBar(ERRORS.serverError);
        return of(fetchBranchesFailure());
      }),
    )),
  ));

  fetchBranch = createEffect(() => this.actions.pipe(
    ofType(fetchBranchRequest),
    mergeMap(({ id }) => this.branchesService.fetchOne(id).pipe(
      map(item => fetchBranchSuccess({ item })),
      catchError(() => {
        this.helpers.openSnackBar(ERRORS.serverError);
        return of(fetchBranchFailure());
      }),
    )),
  ));
}

