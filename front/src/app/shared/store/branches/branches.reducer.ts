import { BranchesState } from '../types';
import { createReducer, on } from '@ngrx/store';
import {
  fetchBranchesFailure,
  fetchBranchesRequest,
  fetchBranchesSuccess,
  fetchBranchFailure,
  fetchBranchRequest,
  fetchBranchSuccess
} from './branches.actions';

const initialState: BranchesState = {
  item: null,
  items: null,
  fetchItemLoading: false,
  fetchItemsLoading: false,
};

export const branchesReducer = createReducer(
  initialState,
  on(fetchBranchesRequest, state => ({ ...state, fetchLoading: true })),
  on(fetchBranchesSuccess, (state, { items }) => ({ ...state, fetchLoading: false, items })),
  on(fetchBranchesFailure, state => ({ ...state, fetchLoading: false })),
  on(fetchBranchRequest, state => ({ ...state, fetchItemLoading: true })),
  on(fetchBranchSuccess, (state, { item }) => ({ ...state, fetchItemLoading: false, item })),
  on(fetchBranchFailure, state => ({ ...state, fetchItemLoading: false })),
);
