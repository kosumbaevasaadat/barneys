import { createAction, props } from '@ngrx/store';
import { Branch } from '../../models/branch.model';

export const fetchBranchesRequest = createAction('[Branches] Fetch Request');
export const fetchBranchesSuccess = createAction('[Branches] Fetch Success', props<{ items: null | Branch[] }>());
export const fetchBranchesFailure = createAction('[Branches] Fetch Failure');

export const fetchBranchRequest = createAction('[Branch] Fetch Request', props<{ id: number }>());
export const fetchBranchSuccess = createAction('[Branch] Fetch Success', props<{ item: null | Branch }>());
export const fetchBranchFailure = createAction('[Branch] Fetch Failure');
