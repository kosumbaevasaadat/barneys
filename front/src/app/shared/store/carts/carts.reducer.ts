import { createReducer, on } from '@ngrx/store';

import { CartsState } from '../types';
import { fetchCartFailure, fetchCartRequest, fetchCartSuccess } from './carts.actions';

const initialState: CartsState = {
  item: null,
  fetchLoading: false,
};

export const cartsReducer = createReducer(
  initialState,
  on(fetchCartRequest, state => ({ ...state, fetchLoading: true })),
  on(fetchCartSuccess, (state, { item }) => ({ ...state, fetchLoading: false, item })),
  on(fetchCartFailure, state => ({ ...state, fetchLoading: false })),
);
