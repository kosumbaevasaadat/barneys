import { catchError, map, mergeMap, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { AppState } from '../types';
import { HelpersService } from '../../services/helpers.service';
import { CartsService } from '../../services/carts.service';
import { fetchCartFailure, fetchCartRequest, fetchCartSuccess } from './carts.actions';
import { ERRORS } from '../../constants';

@Injectable()
export class CartsEffects {
  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    private helpers: HelpersService,
    private cartService: CartsService,
  ) {
  }

  fetchCart = createEffect(() => this.actions.pipe(
    ofType(fetchCartRequest),
    mergeMap(() => this.cartService.fetchOne().pipe(
      map(item => fetchCartSuccess({ item })),
      catchError(() => {
        this.helpers.openSnackBar(ERRORS.serverError);
        return of(fetchCartFailure());
      }),
    )),
  ));
}
