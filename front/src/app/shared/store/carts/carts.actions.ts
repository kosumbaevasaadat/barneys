import { createAction, props } from '@ngrx/store';
import { Cart } from '../../models/cart.model';

export const fetchCartRequest = createAction('[Carts] Fetch Request');
export const fetchCartSuccess = createAction('[Carts] Fetch Success', props<{ item: null | Cart }>());
export const fetchCartFailure = createAction('[Carts] Fetch Failure');
