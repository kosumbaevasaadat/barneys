import { createAction, props } from '@ngrx/store';
import { Menu } from '../../models/menu.model';

export const fetchMenuRequest = createAction('[Menu] Fetch Menu Request', props<{ id: number }>());
export const fetchMenuSuccess = createAction('[Menu] Fetch Menu Success', props<{ item: null | Menu }>());
export const fetchMenuFailure = createAction('[Menu] Fetch Menu Failure');
