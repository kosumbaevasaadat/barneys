import { createReducer, on } from '@ngrx/store';
import { MenuState } from '../types';

import { fetchMenuFailure, fetchMenuRequest, fetchMenuSuccess } from './menu.actions';

const initialState: MenuState = {
  item: null,
  fetchLoading: false,
};

export const menuReducer = createReducer(
  initialState,
  on(fetchMenuRequest, state => ({ ...state, fetchLoading: true })),
  on(fetchMenuSuccess, (state, { item }) => ({ ...state, fetchLoading: false, item })),
  on(fetchMenuFailure, state => ({ ...state, fetchLoading: false })),
);
