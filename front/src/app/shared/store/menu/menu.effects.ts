import { catchError, map, mergeMap, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { AppState } from '../types';
import { HelpersService } from '../../services/helpers.service';
import { fetchMenuFailure, fetchMenuRequest, fetchMenuSuccess } from './menu.actions';
import { MenuService } from '../../services/menu.service';
import { ERRORS } from '../../constants';

@Injectable()
export class MenuEffects {
  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    private helpers: HelpersService,
    private menuService: MenuService,
  ) {
  }

  fetchMenu = createEffect(() => this.actions.pipe(
    ofType(fetchMenuRequest),
    mergeMap(({ id }) => this.menuService.get(id).pipe(
      map(item => fetchMenuSuccess({ item })),
      catchError(() => {
        this.helpers.openSnackBar(ERRORS.serverError);
        return of(fetchMenuFailure());
      })
    ))
  ))
}
