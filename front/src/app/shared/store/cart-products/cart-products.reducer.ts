import { createReducer, on } from '@ngrx/store';

import { CartProductState } from '../types';
import {
  createCartProductFailure,
  createCartProductRequest,
  createCartProductSuccess,
  removeCartProductFailure,
  removeCartProductRequest,
  removeCartProductSuccess,
  updateCartProductFailure,
  updateCartProductRequest,
  updateCartProductSuccess
} from './cart-products.actions';

const initialState: CartProductState = {
  createLoading: false,
  updateLoading: false,
  removeLoading: false,
};

export const cartProductsReducer = createReducer(
  initialState,
  on(createCartProductRequest, state => ({ ...state, createLoading: true })),
  on(createCartProductSuccess, state => ({ ...state, createLoading: false })),
  on(createCartProductFailure, state => ({ ...state, createLoading: false })),
  on(updateCartProductRequest, state => ({ ...state, updateLoading: true })),
  on(updateCartProductSuccess, state => ({ ...state, updateLoading: false })),
  on(updateCartProductFailure, state => ({ ...state, updateLoading: false })),
  on(removeCartProductRequest, state => ({ ...state, removeLoading: true })),
  on(removeCartProductSuccess, state => ({ ...state, removeLoading: false })),
  on(removeCartProductFailure, state => ({ ...state, removeLoading: false })),
);
