import { createAction, props } from '@ngrx/store';
import { CreateCartProductData, UpdateCartProductData } from '../../models/cart-product.model';

export const createCartProductRequest = createAction('[CartProduct] Create Request', props<{ data: CreateCartProductData }>());
export const createCartProductSuccess = createAction('[CartProduct] Create Success');
export const createCartProductFailure = createAction('[CartProduct] Create Failure');

export const updateCartProductRequest = createAction('[CartProduct] Update Request', props<{ id: number, data: UpdateCartProductData }>());
export const updateCartProductSuccess = createAction('[CartProduct] Update Success');
export const updateCartProductFailure = createAction('[CartProduct] Update Failure');

export const removeCartProductRequest = createAction('[CartProduct] Remove Request', props<{ id: number }>());
export const removeCartProductSuccess = createAction('[CartProduct] Remove Success');
export const removeCartProductFailure = createAction('[CartProduct] Remove Failure');

