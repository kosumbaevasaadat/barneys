import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { AppState } from '../types';
import { HelpersService } from '../../services/helpers.service';
import { CartProductService } from '../../services/cart-product.service';
import {
  createCartProductFailure,
  createCartProductRequest,
  createCartProductSuccess,
  removeCartProductFailure,
  removeCartProductRequest,
  removeCartProductSuccess,
  updateCartProductFailure,
  updateCartProductRequest,
  updateCartProductSuccess
} from './cart-products.actions';
import { ERRORS, MESSAGES } from '../../constants';
import { fetchCartRequest } from '../carts/carts.actions';

@Injectable()
export class CartProductsEffects {

  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    private helpers: HelpersService,
    private cartProductService: CartProductService,
  ) {
  }

  createCartProduct = createEffect(() => this.actions.pipe(
    ofType(createCartProductRequest),
    mergeMap(({ data }) => this.cartProductService.create(data).pipe(
      map(() => createCartProductSuccess()),
      tap(() => {
        this.helpers.openSnackBar(MESSAGES.addedToCart);
        this.store.dispatch(fetchCartRequest());
      }),
      catchError(() => {
        this.helpers.openSnackBar(ERRORS.serverError);
        return of(createCartProductFailure());
      })
    ))
  ))

  updateCartProduct = createEffect(() => this.actions.pipe(
    ofType(updateCartProductRequest),
    mergeMap(({ id, data }) => this.cartProductService.update(id, data).pipe(
      map(() => updateCartProductSuccess()),
      catchError(() => {
        this.helpers.openSnackBar(ERRORS.serverError);
        return of(updateCartProductFailure());
      }),
    )),
  ));

  removeCartProduct = createEffect(() => this.actions.pipe(
    ofType(removeCartProductRequest),
    mergeMap(({ id }) => this.cartProductService.remove(id).pipe(
      map(() => removeCartProductSuccess()),
      tap(() => {
        this.store.dispatch(fetchCartRequest());
      }),
      catchError(() => {
        this.helpers.openSnackBar(ERRORS.serverError);
        return of(removeCartProductFailure());
      }),
    )),
  ));

}
