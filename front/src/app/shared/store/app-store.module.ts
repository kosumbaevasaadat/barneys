import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { localStorageSync } from 'ngrx-store-localstorage';

import { UsersEffects } from './users/users.effects';
import { usersReducer } from './users/users.reducer';
import { menuReducer } from './menu/menu.reducer';
import { MenuEffects } from './menu/menu.effects';
import { branchesReducer } from './branches/branches.reducer';
import { BranchesEffects } from './branches/branches.effects';
import { cartsReducer } from './carts/carts.reducer';
import { CartsEffects } from './carts/carts.effects';
import { cartProductsReducer } from './cart-products/cart-products.reducer';
import { CartProductsEffects } from './cart-products/cart-products.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{ users: ['user', 'token'] }],
    rehydrate: true
  })(reducer);
};

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  users: usersReducer,
  menu: menuReducer,
  branches: branchesReducer,
  carts: cartsReducer,
  cartProducts: cartProductsReducer,
};

const effects = [
  UsersEffects,
  MenuEffects,
  BranchesEffects,
  CartsEffects,
  CartProductsEffects,
];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule],
})
export class AppStoreModule {
}
