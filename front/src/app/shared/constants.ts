export const ERRORS = {
  serverError: 'Server error!',
  internetError: 'Internet error!',
}

export const MESSAGES = {
  registerSuccess: 'Registered successful!',
  loginSuccess: 'Login successful!',
  logoutSuccess: 'Logout successful!',
  updated: 'Updated successfully!',
  addedToCart: 'Added to cart!',
}

export const ACTION = {
  ok: 'OK',
  duration: 3000,
  status: 400,
}

export const REGEX = {
  phone: /^\+1( )?\(?\d{3}\) \d{3}-\d{4}$/,
  password: /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&]).{8,64}$/,
}
