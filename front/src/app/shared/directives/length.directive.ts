import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive, Input } from '@angular/core';

export const lengthValidator = (control: AbstractControl, length: number): ValidationErrors | null => {
  const validValue = control.value;
  if (validValue && validValue.toString().length === length) {
    return null;
  }
  return { length: true };
}

@Directive({
  selector: '[appLength]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateLengthDirective,
    multi: true
  }]
})
export class ValidateLengthDirective implements Validator {
  @Input('appLength') length!: number;

  validate(control: AbstractControl): ValidationErrors | null {
    return lengthValidator(control, this.length);
  }
}
