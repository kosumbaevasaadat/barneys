import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive, Input } from '@angular/core';

export function existenceValidator(fields: string[]): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const firstInput = control.get(fields[0]);
    const secondInput = control.get(fields[1]);

    if (firstInput?.value && secondInput?.value
      || !firstInput?.value && !secondInput?.value) {
      return null;
    }

    return { existence: true };
  };
}

@Directive({
  selector: '[appExistence]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateExistenceDirective,
    multi: true,
  }],
})
export class ValidateExistenceDirective implements Validator {
  @Input('appExistence') existFields: string[] = [];

  validate(control: AbstractControl): ValidationErrors | null {
    return existenceValidator(this.existFields)(control);
  }
}
