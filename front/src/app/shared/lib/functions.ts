export const checkIfAllValuesAreNull = (obj: any) => {
  Object.keys(obj).map(key => {
    if (obj[key] === '') {
      obj[key] = null;
    }
  });

  let isNull = true;
  Object.keys(obj).forEach(key => {
    if (obj[key] !== null) isNull = false;
  });

  return isNull ? null : obj;
}
