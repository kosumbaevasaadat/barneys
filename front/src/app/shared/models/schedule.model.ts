import { BaseModel } from './base.model';

export interface Schedule extends BaseModel {
  days: string;
  hours: string;
}
