import { BaseModel } from './base.model';

export interface City extends BaseModel {
  title: string;
}
