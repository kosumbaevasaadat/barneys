import { BaseModel } from './base.model';

export interface Address extends BaseModel {
  [key: string]: any,

  street: string,
  apt: number,
  building: number | null,
  floor: number | null,
}
