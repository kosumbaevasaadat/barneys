import { BaseModel } from './base.model';
import { File } from './file.model';
import { MenuSection } from './menu-section.module';

export interface Menu extends BaseModel {
  file: File;
  sections: MenuSection[];
}
