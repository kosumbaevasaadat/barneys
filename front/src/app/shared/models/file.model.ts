import { BaseModel } from './base.model';

export interface File extends BaseModel {
  name: string;
  fileKey: string;
  contentSize: number;
  contentType: string;
}
