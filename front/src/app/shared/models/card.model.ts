import { BaseModel } from './base.model';

export interface Card extends BaseModel {
  cardName: string,
  cardNumber: number,
  expDate: string,
  cardCode: number,
}
