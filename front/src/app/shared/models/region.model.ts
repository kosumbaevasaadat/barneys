import { BaseModel } from './base.model';

export interface Region extends BaseModel {
  title: string;
}
