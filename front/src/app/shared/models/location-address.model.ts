import { BaseModel } from './base.model';
import { City } from './city.model';
import { State } from './state.model';
import { Region } from './region.model';

export interface LocationAddress extends BaseModel {
  street: string;
  building: number;
  unit: null | number;
  zipCode: number;
  city: City;
  state: State;
  region: Region;
}
