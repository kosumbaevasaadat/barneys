export interface BaseModel {
  id: number,
  status: string,
  createDate: Date,
  updateDate: Date,
}
