import { BaseModel } from './base.model';
import { Dish } from './dish.model';

export interface MenuSection extends BaseModel {
  title: string;
  description: null | string;
  dishes: Dish[];
}
