import { BaseModel } from './base.model';
import { File } from './file.model';
import { LocationAddress } from './location-address.model';
import { Schedule } from './schedule.model';
import { ReceiveMethod } from './receive-method.model';

export interface Branch extends BaseModel {
  title: string;
  description: string;
  phone: string;
  yelpUrl: null | string;
  mainImage: null | File;
  subImage: null | File;
  address: LocationAddress;
  schedules: Schedule[];
  receiveMethods: ReceiveMethod[];
}
