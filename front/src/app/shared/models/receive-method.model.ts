import { BaseModel } from './base.model';

export interface ReceiveMethod extends BaseModel {
  title: string;
}
