import { BaseModel } from './base.model';
import { File } from './file.model';

export interface Dish extends BaseModel {
  title: string;
  description: string;
  price: number;
  file: File;
}
