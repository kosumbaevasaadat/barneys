import { BaseModel } from './base.model';
import { Dish } from './dish.model';

export interface CartProduct extends BaseModel {
  quantity: number;
  product: Dish;
}

export interface CreateCartProductData {
  productId: number;
  quantity: number;
}

export interface UpdateCartProductData {
  quantity: number;
}
