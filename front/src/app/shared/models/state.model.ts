import { BaseModel } from './base.model';

export interface State extends BaseModel {
  title: string;
}
