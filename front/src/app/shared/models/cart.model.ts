import { BaseModel } from './base.model';
import { CartProduct } from './cart-product.model';

export interface Cart extends BaseModel {
  proceedStatus: string;
  products: CartProduct[];
}
