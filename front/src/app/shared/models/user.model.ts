import { BaseModel } from './base.model';
import { Address } from './address.model';
import { Card } from './card.model';

export interface User extends BaseModel {
  email: string,
  firstName: string,
  lastName: string,
  phoneNumber: string,
  paymentMethod: string,
  role: string,
  address: null | Address,
  card: null | Card,
}

export interface UserTokenData {
  user: User,
  token: string,
}

export interface RegisterUserData {
  email: string,
  password: string,
  firstName: string,
  lastName: string,
  phoneNumber: string,
}

export interface LoginUserData {
  email: string,
  password: string,
}

export interface UpdateUserData {
  email: string,
  firstName: string,
  lastName: string,
  phoneNumber: string,
  paymentMethod: string,
  address: null | Address,
  card: null | Card,
}

export interface UpdateEmailData {
  email: string,
  password: string,
}

export interface UpdatePasswordData {
  oldPassword: string,
  newPassword: string,
}

export interface ErrorInterface {
  statusCode: number,
  message: string[],
  error: string,
}
