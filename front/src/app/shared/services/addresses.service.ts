import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Address } from '../models/address.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AddressesService {
  constructor(private http: HttpClient) {
  }

  create(data: Address) {
    return this.http.post(env.API_URL + '/address', data);
  }
}
