import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {
  LoginUserData,
  RegisterUserData,
  UpdateUserData,
  User,
  UserTokenData,
  UpdateEmailData,
  UpdatePasswordData
} from '../models/user.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {
  }

  register(userData: RegisterUserData) {
    return this.http.post<UserTokenData>(env.API_URL + '/auth/register', userData);
  }

  login(userData: LoginUserData) {
    return this.http.post<UserTokenData>(env.API_URL + '/auth/login', userData);
  }

  logout() {
    return this.http.delete(env.API_URL + '/auth/logout');
  }

  getProfile() {
    return this.http.get<User>(env.API_URL + '/user/me');
  }

  update(data: UpdateUserData) {
    return this.http.patch<User>(env.API_URL + '/user/', data);
  }

  updateEmail(data: UpdateEmailData) {
    return this.http.patch<User>(env.API_URL + '/user/email', data);
  }

  updatePassword(data: UpdatePasswordData) {
    return this.http.patch<User>(env.API_URL + '/user/password', data);
  }
}
