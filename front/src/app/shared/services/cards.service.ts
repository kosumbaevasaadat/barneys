import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Card } from '../models/card.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CardsService {
  constructor(private http: HttpClient) {
  }

  create(data: Card) {
    return this.http.post(env.API_URL + '/card', data);
  }
}
