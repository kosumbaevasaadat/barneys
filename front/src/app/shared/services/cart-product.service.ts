import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CreateCartProductData, UpdateCartProductData } from '../models/cart-product.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CartProductService {

  constructor(
    private http: HttpClient,
  ) {
  }

  create(data: CreateCartProductData) {
    return this.http.post(env.API_URL + '/cart-product', data);
  }

  update(id: number, data: UpdateCartProductData) {
    return this.http.patch(env.API_URL + '/cart-product/' + id, data);
  }

  remove(id: number) {
    return this.http.delete(env.API_URL + '/cart-product/' + id);
  }

}
