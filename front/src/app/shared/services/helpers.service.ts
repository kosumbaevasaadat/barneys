import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { ActionType } from '@ngrx/store';
import { catchError, of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

import { ACTION, ERRORS } from '../constants';

@Injectable({
  providedIn: 'root',
})
export class HelpersService {

  constructor(private snackbar: MatSnackBar) {
  }

  openSnackBar(message: string, action?: string, config?: MatSnackBarConfig) {
    if (!config || !config.duration) {
      config = { ...config, duration: ACTION.duration };
    }

    if (!action) {
      action = ACTION.ok;
    }

    return this.snackbar.open(message, action, config);
  }

  catchServerError(action: ActionType<any>) {
    return catchError(reqErr => {
      let validationError = null;

      if (reqErr instanceof HttpErrorResponse && reqErr.status === ACTION.status) {
        validationError = reqErr.error;
      } else {
        this.snackbar.open(ERRORS.serverError, ACTION.ok, { duration: ACTION.duration });
      }

      return of(action({ error: validationError }));
    });
  }
}
