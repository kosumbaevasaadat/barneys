import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment as env } from '../../../environments/environment';
import { Menu } from '../models/menu.model';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  constructor(
    private http: HttpClient,
  ) {
  }

  get(id: number) {
    return this.http.get<Menu>(`${ env.API_URL }/menu/${ id }`);
  }
}
