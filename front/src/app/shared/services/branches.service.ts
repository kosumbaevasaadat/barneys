import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment as env } from '../../../environments/environment';
import { Branch } from '../models/branch.model';

@Injectable({
  providedIn: 'root'
})
export class BranchesService {
  constructor(private http: HttpClient) {
  }

  fetchAll() {
    return this.http.get<Branch[]>(`${ env.API_URL }/branch`);
  }

  fetchOne(id: number) {
    return this.http.get<Branch>(`${ env.API_URL }/branch/${ id }`);
  }
}
