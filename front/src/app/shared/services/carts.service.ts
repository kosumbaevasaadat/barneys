import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Cart } from '../models/cart.model';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CartsService {

  constructor(private http: HttpClient) {
  }

  fetchOne() {
    return this.http.get<Cart>(env.API_URL + '/cart');
  }

}
