import { catchError, Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

import { AppState } from '../store/types';
import { HelpersService } from '../services/helpers.service';
import { logoutUser } from '../store/users/users.actions';
import { ERRORS } from '../constants';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  token: Observable<null | string>;
  tokenData: null | string = null;

  constructor(private store: Store<AppState>,
              private helpers: HelpersService,
              private router: Router,) {
    this.token = store.select(state => state.users.token);
    this.token.subscribe(token => {
      this.tokenData = token ? token : null;
    });
  }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.tokenData) {
      req = req.clone({
        setHeaders: { 'Authorization': `Bearer ${ this.tokenData }` },
      });
    }

    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 0) {
          this.helpers.openSnackBar(ERRORS.internetError);
        }

        if (error.status === 401) {
          this.store.dispatch(logoutUser());
          void this.router.navigate(['/login']);
        }

        return throwError(() => error);
      })
    );
  }
}
