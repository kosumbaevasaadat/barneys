import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <app-short-toolbar>
      <div>
        <button mat-icon-button [matMenuTriggerFor]="menu">
          <mat-icon class="icon">person</mat-icon>
        </button>
        <mat-menu #menu="matMenu">
          <a mat-menu-item routerLink="/profiles/id/edit">Profile</a>
          <button mat-menu-item (click)="logout()">Logout</button>
        </mat-menu>
      </div>
    </app-short-toolbar>
    <mat-card>
      <mat-card-title>Not found!</mat-card-title>
    </mat-card>
  `,
  styles: [
    `mat-card
       width: 300px
       height: 64px
       background: #FFCCCB
       color: #FF0000
       font-size: 22px
       text-align: center
       padding: 16px
       margin: 100px auto 0`,
    `mat-card-title
       font-size: 25px`,
  ]

})
export class NotFoundComponent {
  logout() {

  }
}
