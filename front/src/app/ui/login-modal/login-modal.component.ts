import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { ErrorInterface, User } from '../../shared/models/user.model';
import { AppState } from '../../shared/store/types';
import { loginUserRequest } from '../../shared/store/users/users.actions';
import { RegisterModalComponent } from '../register-modal/register-modal.component';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.sass']
})
export class LoginModalComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  error: Observable<null | ErrorInterface>;
  loading: Observable<boolean>;
  user: Observable<null | User>;
  userSub!: Subscription;

  constructor(private store: Store<AppState>,
              private dialogRef: MatDialogRef<LoginModalComponent>,
              private dialog: MatDialog,
  ) {
    this.error = store.select(state => state.users.loginError);
    this.loading = store.select(state => state.users.loginLoading);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.userSub = this.user.subscribe(user => {
      if (user) {
        this.onCloseClick();
      }
    });
  }

  onSubmit() {
    this.store.dispatch(loginUserRequest({ userData: this.form.form.value }));
  }

  onCloseClick() {
    this.dialogRef.close();
  }

  openRegisterModal() {
    this.dialogRef.close();
    this.dialog.open(RegisterModalComponent, {
      autoFocus: false,
    });
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }
}
