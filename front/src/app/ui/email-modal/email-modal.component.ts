import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { NgForm } from '@angular/forms';

import { AppState } from '../../shared/store/types';
import { updateEmailRequest } from '../../shared/store/users/users.actions';
import { ErrorInterface } from '../../shared/models/user.model';

@Component({
  selector: 'app-email-modal',
  templateUrl: './email-modal.component.html',
  styleUrls: ['./email-modal.component.sass']
})
export class EmailModalComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;

  error: Observable<null | ErrorInterface>;
  errorSub!: Subscription;
  errorData: null | ErrorInterface = null;

  updateLoading: Observable<boolean>;

  constructor(public dialogRef: MatDialogRef<EmailModalComponent>,
              private store: Store<AppState>) {
    this.error = store.select(state => state.users.updateEmailError);
    this.updateLoading = store.select(state => state.users.updateEmailLoading);
  }

  ngOnInit(): void {
    this.errorSub = this.error.subscribe(error => {
      this.errorData = error ? error : null;
    });
  }

  onCloseClick() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.store.dispatch(updateEmailRequest({ data: this.form.form.value }));
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
  }
}
