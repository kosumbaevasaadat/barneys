import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';

import { AppState } from '../../shared/store/types';
import { CreateCartProductData } from '../../shared/models/cart-product.model';
import { createCartProductRequest } from '../../shared/store/cart-products/cart-products.actions';
import { Dish } from '../../shared/models/dish.model';

@Component({
  selector: 'app-card-modal',
  templateUrl: './card-modal.component.html',
  styleUrls: ['./card-modal.component.sass']
})
export class CardModalComponent {
  amount = 1;

  constructor(
    public dialogRef: MatDialogRef<CardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Dish,
    private store: Store<AppState>,
  ) {
  }

  increaseAmount() {
    this.amount += 1;
  }

  decreaseAmount() {
    this.amount -= 1;
  }

  onCloseClick() {
    this.dialogRef.close();
  }

  addToCart() {
    const data: CreateCartProductData = {
      productId: this.data.id,
      quantity: this.amount,
    };
    this.store.dispatch(createCartProductRequest({ data }));
    this.dialogRef.close();
  }

}
