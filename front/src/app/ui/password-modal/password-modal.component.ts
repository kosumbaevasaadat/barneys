import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { NgForm } from '@angular/forms';

import { AppState } from '../../shared/store/types';
import { updatePasswordRequest } from '../../shared/store/users/users.actions';
import { Observable, Subscription } from 'rxjs';
import { ErrorInterface } from '../../shared/models/user.model';

@Component({
  selector: 'app-password-modal',
  templateUrl: './password-modal.component.html',
  styleUrls: ['./password-modal.component.sass']
})
export class PasswordModalComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;

  error: Observable<null | ErrorInterface>;
  errorSub!: Subscription;
  errorData: null | ErrorInterface = null;

  updateLoading: Observable<boolean>;

  constructor(public dialogRef: MatDialogRef<PasswordModalComponent>,
              private store: Store<AppState>) {
    this.error = store.select(state => state.users.updatePasswordError);
    this.updateLoading = store.select(state => state.users.updatePasswordLoading);
  }

  ngOnInit() {
    this.errorSub = this.error.subscribe(error => {
      this.errorData = error ? error : null;
    });
  }

  onCloseClick() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.store.dispatch(updatePasswordRequest({ data: this.form.form.value }));
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
  }
}
