import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {
  posts = [
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1578193446853-TMQ9AJ6MW9VR2UKPNVQF/image-asset.jpeg?format=300w',
      link: 'https://www.instagram.com/p/B66-lNZFHtX/',
    },
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1578013454839-M8C1SAGHPS9AYR89OH69/image-asset.jpeg?format=500w',
      link: 'https://www.instagram.com/p/B61lg9gF6ZZ/',
    },
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1577843039238-OHSUN5BMRKKATMB463TE/image-asset.jpeg?format=300w',
      link: 'https://www.instagram.com/p/B6wgKOFl7mY/',
    },
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1577421534204-LMSBNOTO8H1V3V0XI7SB/image-asset.jpeg?format=300w',
      link: 'https://www.instagram.com/p/B6j71SMFkLw/',
    },
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1577212249095-LHL815S2Q9G5QR5U06QL/image-asset.jpeg?format=500w',
      link: 'https://www.instagram.com/p/B6dwlVllVOF/',
    },
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1577147926212-LTJQYEEJH8ASNCBCHH4C/image-asset.jpeg?format=300w',
      link: 'https://www.instagram.com/p/B6byRqiFMjr/',
    },
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1577059585464-1KYYWC9UTSX9M3KC6DC7/image-asset.jpeg?format=300w',
      link: 'https://www.instagram.com/p/B6ZHQ_KlxT1/',
    },
    {
      url: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1576990833785-124V7W4YDMXP6VX0TJBI/image-asset.jpeg?format=300w',
      link: 'https://www.instagram.com/p/B6XEdiYlPxG/',
    },
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
