import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { CartProduct } from '../../../shared/models/cart-product.model';
import { AppState } from '../../../shared/store/types';
import {
  removeCartProductRequest,
  updateCartProductRequest
} from '../../../shared/store/cart-products/cart-products.actions';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.sass']
})
export class CartItemComponent implements OnInit {
  @Input() data!: CartProduct;

  quantity = 1;

  constructor(
    private store: Store<AppState>,
  ) {
  }

  ngOnInit(): void {
    this.quantity = this.data.quantity;
  }

  increaseAmount() {
    this.quantity += 1;
    this.store.dispatch(updateCartProductRequest({ id: this.data.id, data: { quantity: this.quantity } }));
  }

  decreaseAmount() {
    if (this.quantity === 1) {
      return this.store.dispatch(removeCartProductRequest({ id: this.data.id }));
    }

    this.quantity -= 1;
    this.store.dispatch(updateCartProductRequest({ id: this.data.id, data: { quantity: this.quantity } }));
  }

  onDelete() {
    this.store.dispatch(removeCartProductRequest({ id: this.data.id }));
  }

}
