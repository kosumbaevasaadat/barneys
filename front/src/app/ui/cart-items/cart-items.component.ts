import { Component, Input } from '@angular/core';
import { Cart } from '../../shared/models/cart.model';

@Component({
  selector: 'app-cart-items',
  templateUrl: './cart-items.component.html',
  styleUrls: ['./cart-items.component.sass']
})
export class CartItemsComponent {
  @Input() data!: Cart;
}
