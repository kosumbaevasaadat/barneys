import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { CardModalComponent } from '../../card-modal/card-modal.component';
import { Dish } from '../../../shared/models/dish.model';

@Component({
  selector: 'app-order-menu-item',
  templateUrl: './order-menu-item.component.html',
  styleUrls: ['./order-menu-item.component.sass']
})
export class OrderMenuItemComponent implements OnInit {
  @Input() data!: Dish;

  constructor(
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    this.dialog.open(CardModalComponent, {
      autoFocus: false,
      data: this.data,
    });
  }

  onRemoveClick(event: Event) {
    event.stopPropagation();
  }

}
