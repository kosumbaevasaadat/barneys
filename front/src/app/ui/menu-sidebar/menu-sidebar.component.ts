import { AfterViewInit, Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-sidebar',
  templateUrl: './menu-sidebar.component.html',
  styleUrls: ['./menu-sidebar.component.sass']
})
export class MenuSidebarComponent implements AfterViewInit {
  @Input() link: string | undefined;

  slides!: HTMLCollectionOf<HTMLElement>;
  step = 8;
  startIndex = 0;
  lastIndex = 0;

  links = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  currLink = '';

  constructor(private route: Router) {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.currLink = this.route.url.substring(this.route.url.indexOf('#') + 1);
    });
    this.slides = document.getElementsByClassName('menu-slide-item') as HTMLCollectionOf<HTMLElement>;
    this.showSlides();
  }

  reduceSlides() {
    const saveStart = this.startIndex;
    const saveLast = this.lastIndex;

    if (this.startIndex - this.step < 0) {
      this.startIndex = 0;
      this.lastIndex = this.step - 1;
    } else if (this.lastIndex - this.step >= 0) {
      this.lastIndex = this.startIndex;
      this.startIndex -= this.step - 1;
    }

    this.drawSlides(saveStart, saveLast);
  }

  showSlides() {
    const saveStart = this.startIndex;
    const saveLast = this.lastIndex;

    if (this.lastIndex + this.step >= this.slides.length) {
      this.startIndex = this.links.length - this.step;
      this.lastIndex = this.slides.length - 1;

    }
    if (this.lastIndex + this.step <= this.slides.length) {
      this.startIndex = this.lastIndex;
      this.lastIndex += this.step - 1;
    }

    this.drawSlides(saveStart, saveLast);
  }

  drawSlides(start: number, end: number) {
    for (let i = start; i <= end; i++) {
      this.slides[i].style.display = 'none';
    }

    for (let i = this.startIndex; i <= this.lastIndex; i++) {
      this.slides[i].style.display = 'block';
    }
  }

}
