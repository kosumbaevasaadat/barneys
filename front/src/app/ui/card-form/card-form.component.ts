import { Observable, Subscription } from 'rxjs';
import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import { User } from '../../shared/models/user.model';
import { AppState } from '../../shared/store/types';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.sass']
})
export class CardFormComponent implements AfterViewInit, OnDestroy {
  @Input() paymentMethod: null | string = null;
  @Output() formValidity = new EventEmitter<string>();

  @ViewChild('f') form!: NgForm;

  user: Observable<null | User>;
  userSub!: Subscription;
  userData: null | User = null;

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }

  ngAfterViewInit() {
    this.userSub = this.user.subscribe(user => {
      if (user) {
        this.userData = user;
      }
    });
    setTimeout(() => {
      if (this.userData?.card) {
        this.form.form.patchValue(this.userData.card);
      }

      if (this.form.statusChanges) {
        this.form.statusChanges.subscribe(status => {
          this.formValidity.emit(status);
        });
      }
    });
  }

  modifyExpDateInput(event: any) {
    if (event.target.value.length === 2) {
      event.target.value = event.target.value + '/';
    } else if (event.target.value.length === 3 && event.target.value.charAt(2) === '/') {
      event.target.value = event.target.value.replace('/', '');
    }
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}
