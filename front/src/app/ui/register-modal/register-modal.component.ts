import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { ErrorInterface, User } from '../../shared/models/user.model';
import { AppState } from '../../shared/store/types';
import { registerUserRequest } from '../../shared/store/users/users.actions';
import { LoginModalComponent } from '../login-modal/login-modal.component';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.sass']
})
export class RegisterModalComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  error: Observable<null | ErrorInterface>;
  errorSub!: Subscription;
  errorData: null | string[] = null;
  loading: Observable<boolean>;
  user: Observable<null | User>;
  userSub!: Subscription;

  constructor(
    private store: Store<AppState>,
    private dialogRef: MatDialogRef<RegisterModalComponent>,
    private dialog: MatDialog,
  ) {
    this.error = store.select(state => state.users.registerError);
    this.loading = store.select(state => state.users.registerLoading);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.userSub = this.user.subscribe(user => {
      if (user) {
        this.onCloseClick();
      }
    });

    this.errorSub = this.error.subscribe(err => {
      if (err) {
        this.errorData = err.message;
      } else {
        this.errorData = null;
      }
    });
  }

  onCloseClick() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.store.dispatch(registerUserRequest({ userData: this.form.value }));
  }

  openLoginModal() {
    this.dialogRef.close();
    this.dialog.open(LoginModalComponent, {
      autoFocus: false,
    });
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
    this.errorSub.unsubscribe();
  }
}
