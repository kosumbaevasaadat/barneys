import { Observable, Subscription } from 'rxjs';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import { User } from '../../shared/models/user.model';
import { AppState } from '../../shared/store/types';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.sass']
})
export class ContactFormComponent implements OnInit, OnDestroy {
  @Output() formValidity = new EventEmitter<string>();
  @ViewChild('f') form!: NgForm;

  user: Observable<null | User>;
  userSub!: Subscription;
  userData: null | User = null;

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.userSub = this.user.subscribe(user => {
      if (user) {
        this.userData = user;
      }
    });

    setTimeout(() => {
      if (this.userData) {
        this.form.form.patchValue(this.userData);
      }

      if (this.form.statusChanges) {
        this.form.statusChanges.subscribe(status => {
          this.formValidity.emit(status);
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }
}
