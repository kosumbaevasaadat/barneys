import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { MatBadgeModule } from '@angular/material/badge';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LocationComponent } from './pages/locations/location/location.component';
import { HomeComponent } from './pages/home/home.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { BranchComponent } from './pages/branch/branch.component';
import { MenuComponent } from './pages/menu/menu.component';
import { MenuItemComponent } from './pages/menu/menu-item/menu-item.component';
import { OrderMenuItemComponent } from './ui/order-menu/order-menu-item/order-menu-item.component';
import { MenuSidebarComponent } from './ui/menu-sidebar/menu-sidebar.component';
import { LocationDetailsComponent } from './pages/location-details/location-details.component';
import { MapComponent } from './pages/map/map.component';
import { MapLocationComponent } from './pages/map/map-location/map-location.component';
import { AboutComponent } from './pages/about/about.component';
import { OrderMenuComponent } from './ui/order-menu/order-menu.component';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { SidebarComponent } from './ui/sidebar/sidebar.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ShortToolbarComponent } from './ui/short-toolbar/short-toolbar.component';
import { ShortFooterComponent } from './ui/short-footer/short-footer.component';
import { CardModalComponent } from './ui/card-modal/card-modal.component';
import { CartComponent } from './pages/cart/cart.component';
import { CartItemsComponent } from './ui/cart-items/cart-items.component';
import { CartItemComponent } from './ui/cart-items/cart-item/cart-item.component';
import { ValidatePhoneDirective } from './shared/directives/validate-phone.directive';
import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';
import { NotFoundComponent } from './not-found.component';
import { CardFormComponent } from './ui/card-form/card-form.component';
import { ContactFormComponent } from './ui/contact-form/contact-form.component';
import { AddressFormComponent } from './ui/address-form/address-form.component';
import { PasswordModalComponent } from './ui/password-modal/password-modal.component';
import { EmailModalComponent } from './ui/email-modal/email-modal.component';
import { ValidateIdenticalDirective } from './shared/directives/validate-identical.directive';
import { EditDishComponent } from './pages/edit-dish/edit-dish.component';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { AppStoreModule } from './shared/store/app-store.module';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';
import { UserTypeDirective } from './shared/directives/user-type.directive';
import { ValidatePasswordDirective } from './shared/directives/validate-password.directive';
import { ValidateExistenceDirective } from './shared/directives/validate-existence.directive';
import { LoginModalComponent } from './ui/login-modal/login-modal.component';
import { RegisterModalComponent } from './ui/register-modal/register-modal.component';
import { ValidateLengthDirective } from './shared/directives/length.directive';
import { ImagePipe } from './shared/pipes/image.pipe';
import { HasRolesDirective } from './shared/directives/has-roles.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LocationsComponent,
    LocationComponent,
    LocationDetailsComponent,
    MenuComponent,
    MenuItemComponent,
    AboutComponent,
    MapComponent,
    MapLocationComponent,
    BranchComponent,
    MenuSidebarComponent,
    OrderMenuComponent,
    OrderMenuItemComponent,
    CenteredCardComponent,
    ToolbarComponent,
    FooterComponent,
    SidebarComponent,
    ContactComponent,
    ShortToolbarComponent,
    ShortFooterComponent,
    CardModalComponent,
    CartComponent,
    CartItemsComponent,
    CartItemComponent,
    ValidatePhoneDirective,
    EditProfileComponent,
    NotFoundComponent,
    CardFormComponent,
    ContactFormComponent,
    AddressFormComponent,
    PasswordModalComponent,
    EmailModalComponent,
    ValidateIdenticalDirective,
    EditDishComponent,
    FileInputComponent,
    UserTypeDirective,
    ValidatePasswordDirective,
    ValidateExistenceDirective,
    LoginModalComponent,
    RegisterModalComponent,
    ValidateLengthDirective,
    ImagePipe,
    HasRolesDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FlexModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatBadgeModule,
    MatRadioModule,
    AppStoreModule,
    MatSnackBarModule,
    HttpClientModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule {
}
