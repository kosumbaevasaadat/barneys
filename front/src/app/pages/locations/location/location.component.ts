import { Component, Input, OnInit } from '@angular/core';
import { Branch } from '../../../shared/models/branch.model';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.sass']
})
export class LocationComponent implements OnInit {
  @Input() data!: Branch;

  constructor() {
  }

  ngOnInit(): void {
  }

}
