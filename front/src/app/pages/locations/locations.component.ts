import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Branch } from '../../shared/models/branch.model';
import { AppState } from '../../shared/store/types';
import { fetchBranchesRequest } from '../../shared/store/branches/branches.actions';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.sass']
})
export class LocationsComponent implements OnInit, OnDestroy {
  branches: Observable<null | Branch[]>;
  fetchLoading: Observable<boolean>;
  branchesSub!: Subscription;
  branchesData!: null | Branch[];

  bayArea: Branch[] = [];
  losAngeles: Branch[] = [];

  constructor(private store: Store<AppState>) {
    this.branches = store.select(state => state.branches.items);
    this.fetchLoading = store.select(state => state.branches.fetchItemsLoading);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchBranchesRequest());

    this.branchesSub = this.branches.subscribe(data => {
      if (!data) return;
      this.branchesData = data;

      this.bayArea = this.branchesData.filter((item) => {
        return item.address.region.title === 'Bay Area & San Francisco';
      });

      this.losAngeles = this.branchesData.filter((item) => {
        return item.address.region.title === 'Los Angeles';
      });
    });
  }

  ngOnDestroy(): void {
    this.branchesSub.unsubscribe();
  }

}
