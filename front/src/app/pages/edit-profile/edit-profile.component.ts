import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';

import { EmailModalComponent } from '../../ui/email-modal/email-modal.component';
import { PasswordModalComponent } from '../../ui/password-modal/password-modal.component';
import { AppState } from '../../shared/store/types';
import { ErrorInterface, User } from '../../shared/models/user.model';
import { checkIfAllValuesAreNull } from '../../shared/lib/functions';
import { updateUserRequest } from '../../shared/store/users/users.actions';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.sass']
})
export class EditProfileComponent implements OnInit, OnDestroy {
  @ViewChild('contactForm') contactForm!: NgForm;
  @ViewChild('addressForm') addressForm!: NgForm;
  @ViewChild('cardForm') cardForm!: NgForm;

  user: Observable<null | User>;
  userSub!: Subscription;
  userData: null | User = null;

  updateUserErr: Observable<null | ErrorInterface>;
  updateUserErrSub!: Subscription;
  updateUserErrData: null | ErrorInterface = null;
  updateLoading: Observable<boolean>;

  isContactFormValid = true;
  isAddressFormValid = true;
  isCardFormValid = true;

  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
    this.updateUserErr = store.select(state => state.users.updateError);
    this.updateLoading = store.select(state => state.users.updateLoading);
  }

  ngOnInit() {
    this.userSub = this.user.subscribe(user => {
      if (user) {
        this.userData = user;
      }
    });
    this.updateUserErrSub = this.updateUserErr.subscribe(err => {
      this.updateUserErrData = err ? err : null;
    })
  }

  openEmailDialog() {
    this.dialog.open(EmailModalComponent, { autoFocus: false });
  }

  openPasswordDialog() {
    this.dialog.open(PasswordModalComponent, { autoFocus: false });
  }

  updateContactFormValidity(valid: string) {
    this.isContactFormValid = valid === 'VALID';
  }

  updateAddressFormValidity(valid: string) {
    this.isAddressFormValid = valid === 'VALID';
  }

  updateCardFormValidity(valid: string) {
    this.isCardFormValid = valid === 'VALID';
  }

  onSubmit() {
    Object.keys(this.contactForm.form.value).map(key => {
      if (this.contactForm.form.value[key] === '') {
        this.contactForm.form.value[key] = null;
      }
    });

    const address = checkIfAllValuesAreNull(this.addressForm.form.value);
    let card = null;
    if (this.cardForm) {
      card = checkIfAllValuesAreNull(this.cardForm.form.value);
    }

    if (this.userData) {
      this.store.dispatch(updateUserRequest({
        userData: { ...this.contactForm.form.value, address, card }
      }));
    }
  }

  ngOnDestroy(): void {
    this.updateUserErrSub.unsubscribe();
  }
}
