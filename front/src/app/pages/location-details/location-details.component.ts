import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

import { Branch } from '../../shared/models/branch.model';
import { AppState } from '../../shared/store/types';
import { fetchBranchRequest } from '../../shared/store/branches/branches.actions';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.sass']
})
export class LocationDetailsComponent implements OnInit, OnDestroy {
  branch: Observable<null | Branch>;
  fetchLoading: Observable<boolean>;
  branchSub!: Subscription;
  branchData!: null | Branch;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
  ) {
    this.branch = store.select(state => state.branches.item);
    this.fetchLoading = store.select(state => state.branches.fetchItemLoading);
  }

  ngOnInit(): void {
    this.route.params.subscribe(data => {
      const id = parseInt(data['id']);
      this.store.dispatch(fetchBranchRequest({ id }));
    });

    this.branchSub = this.branch.subscribe(data => {
      if (!data) return;
      this.branchData = data;
    });
  }

  ngOnDestroy(): void {
    this.branchSub.unsubscribe();
  }

}
