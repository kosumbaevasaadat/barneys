import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit-dish',
  templateUrl: './edit-dish.component.html',
  styleUrls: ['./edit-dish.component.sass']
})
export class EditDishComponent {
  @ViewChild('f') form!: NgForm;

  constructor() {
  }

  onSubmit() {

  }

  logout() {

  }
}
