import { Component, Input, OnInit } from '@angular/core';

import { Branch } from '../../../shared/models/branch.model';

@Component({
  selector: 'app-map-location',
  templateUrl: './map-location.component.html',
  styleUrls: ['./map-location.component.sass']
})
export class MapLocationComponent implements OnInit {
  @Input() data!: Branch;

  constructor() { }

  ngOnInit(): void {
  }

}
