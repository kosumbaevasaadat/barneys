import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Branch } from '../../shared/models/branch.model';
import { AppState } from '../../shared/store/types';
import { fetchBranchesRequest } from '../../shared/store/branches/branches.actions';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit, OnDestroy {
  branches: Observable<null | Branch[]>;
  fetchLoading: Observable<boolean>;
  branchesSub!: Subscription;
  branchesData!: null | Branch[];

  constructor(private store: Store<AppState>) {
    this.branches = store.select(state => state.branches.items);
    this.fetchLoading = store.select(state => state.branches.fetchItemsLoading);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchBranchesRequest());

    this.branchesSub = this.branches.subscribe(data => {
      if (!data) return;
      this.branchesData = data;
    });
  }

  ngOnDestroy(): void {
    this.branchesSub.unsubscribe();
  }

}
