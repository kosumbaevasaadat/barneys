import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../shared/store/types';
import { Menu } from '../../shared/models/menu.model';
import { fetchMenuRequest } from '../../shared/store/menu/menu.actions';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit, OnDestroy {
  menu: Observable<null | Menu>;
  fetchLoading: Observable<boolean>;
  menuSub!: Subscription;
  menuData!: null | Menu;

  constructor(
    private store: Store<AppState>
  ) {
    this.menu = store.select(state => state.menu.item);
    this.fetchLoading = store.select(state => state.menu.fetchLoading);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchMenuRequest({ id: 1 }));

    this.menuSub = this.menu.subscribe(data => {
      this.menuData = data;
    })
  }

  ngOnDestroy(): void {
    this.menuSub.unsubscribe();
  }

}
