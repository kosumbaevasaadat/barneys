import { Component, Input, OnInit } from '@angular/core';
import { Dish } from '../../../shared/models/dish.model';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.sass']
})
export class MenuItemComponent implements OnInit {
  @Input() data!: Dish;

  constructor() { }

  ngOnInit(): void {
  }

}
