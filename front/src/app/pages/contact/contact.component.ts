import { Component } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent {
  locations = ['Brentwood', 'Santa Monica', 'San Francisco', 'Berkeley', 'College Ave.', 'Berkeley - Solano', 'Piedmont'];

  sendMessage() {

  }
}
