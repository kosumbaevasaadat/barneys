import { Observable, Subscription } from 'rxjs';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import { Branch } from '../../shared/models/branch.model';
import { AppState } from '../../shared/store/types';
import { Cart } from '../../shared/models/cart.model';
import { fetchCartRequest } from '../../shared/store/carts/carts.actions';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  @ViewChild('contactForm') contactForm!: NgForm;
  @ViewChild('addressForm') addressForm!: NgForm;
  @ViewChild('cardForm') cardForm!: NgForm;

  branch: Observable<null | Branch>;
  branchSub!: Subscription;
  branchData!: null | Branch;

  cart: Observable<null | Cart>;
  cartFetchLoading: Observable<boolean>;
  cartSub!: Subscription;
  cartData: null | Cart = null;

  constructor(private store: Store<AppState>) {
    this.branch = store.select(state => state.branches.item);
    this.cart = store.select(state => state.carts.item);
    this.cartFetchLoading = store.select(state => state.carts.fetchLoading);
  }

  ngOnInit(): void {
    this.branchSub = this.branch.subscribe(data => {
      this.branchData = data;
    });

    this.store.dispatch(fetchCartRequest());
    this.cartSub = this.cart.subscribe(data => {
      this.cartData = data;
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.form.controls['receiptType'].setValue('delivery');
    });
  }

  onSubmit() {

  }

  ngOnDestroy(): void {
    this.branchSub.unsubscribe();
    this.cartSub.unsubscribe();
  }

}
