import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';

import { AppState } from '../../shared/store/types';
import { logoutUserRequest } from '../../shared/store/users/users.actions';
import { RegisterModalComponent } from '../../ui/register-modal/register-modal.component';
import { LoginModalComponent } from '../../ui/login-modal/login-modal.component';
import { Branch } from '../../shared/models/branch.model';
import { fetchBranchRequest } from '../../shared/store/branches/branches.actions';
import { Schedule } from '../../shared/models/schedule.model';
import { Cart } from '../../shared/models/cart.model';
import { fetchCartRequest } from '../../shared/store/carts/carts.actions';
import { User } from '../../shared/models/user.model';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.sass']
})
export class BranchComponent implements OnInit, OnDestroy {
  branch: Observable<null | Branch>;
  fetchLoading: Observable<boolean>;
  branchSub!: Subscription;
  branchData!: null | Branch;

  cart: Observable<null | Cart>;
  cartFetchLoading: Observable<boolean>;
  cartSub!: Subscription;
  cartData: null | Cart = null;

  user: Observable<null | User>;
  userSub!: Subscription;

  schedule: Schedule | null = null;

  constructor(
    private store: Store<AppState>,
    private dialog: MatDialog,
    private route: ActivatedRoute,
  ) {
    this.branch = store.select(state => state.branches.item);
    this.fetchLoading = store.select(state => state.branches.fetchItemLoading);
    this.cart = store.select(state => state.carts.item);
    this.cartFetchLoading = store.select(state => state.carts.fetchLoading);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.route.params.subscribe(data => {
      const id = parseInt(data['id']);
      this.store.dispatch(fetchBranchRequest({ id }));
    });

    this.branchSub = this.branch.subscribe(data => {
      if (!data) return;
      this.branchData = data;
      this.getHours();
      this.isOpen();
    });

    this.userSub = this.user.subscribe(user => {
      if (user) {
        this.store.dispatch(fetchCartRequest());
        this.cartSub = this.cart.subscribe(data => {
          this.cartData = data;
        });
      }
    });
  }

  getHours() {
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let now = new Date().toLocaleDateString('en-us', { weekday: 'long' });

    this.branchData?.schedules.forEach(s => {
      const days = s.days.split(/[-&]/).map(d => {
        return d.trim();
      });

      const fullDays = weekdays.slice(weekdays.indexOf(days[0]), weekdays.indexOf(days[1]) + 1);

      if (fullDays.indexOf(now) !== -1) {
        this.schedule = s;
      }
    });
  }

  isOpen() {
    if (!this.schedule) return false;

    const now = new Date();

    const scheduleTime = this.schedule.hours.split('-');

    return now > new Date(this.getDate() + ' ' + scheduleTime[0])
      && now < new Date(this.getDate() + ' ' + scheduleTime[1]);
  }

  getDate() {
    const date = new Date();

    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    return `${ month }/${ day }/${ year }`;
  }

  getProductsQuantity() {
    let res = 0;

    if (this.cartData) {
      for (const p of this.cartData.products) {
        res += p.quantity;
      }
    }

    if (!res) return null;
    return res;
  }

  openRegisterModal() {
    this.dialog.open(RegisterModalComponent, {
      autoFocus: false,
    });
  }

  openLoginModal() {
    this.dialog.open(LoginModalComponent, {
      autoFocus: false,
    });
  }

  logout() {
    this.store.dispatch(logoutUserRequest());
  }

  ngOnDestroy(): void {
    this.branchSub.unsubscribe();
    this.userSub.unsubscribe();
    if (this.cartSub) {
      this.cartSub.unsubscribe();
    }
  }

}
