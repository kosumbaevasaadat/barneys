import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements AfterViewInit, OnDestroy {
  @ViewChild('matHorizontalStepper') stepper!: MatStepper;
  currentStep = 0;
  currentInterval!: NodeJS.Timer;

  slides = [
    { path: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1573018247756-UJF1RPWSB0QS4RG4G191/Milkshakes+Perfected-01.png?format=1500w' },
    { path: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1573019124344-EWZQHOGEW66ZEWO4F1X5/Options-01.png?format=1500w ' },
    { path: 'https://images.squarespace-cdn.com/content/v1/5dc23c7796161954581e00b3/1573012371350-MFBMPAJI0HCMQ1ZZIDZX/Header+1-01.png?format=1500w' },];

  constructor() {
  }

  ngAfterViewInit() {
    this.autoplay(this.stepper);
  }

  autoplay(stepper: MatStepper) {
    this.currentInterval = setInterval(() => {
      this.currentStep += 1;
      if (this.currentStep === this.slides.length) {
        stepper.reset();
        this.currentStep = 0;
      } else {
        stepper.next();
      }
    }, 3000);
  }

  ngOnDestroy(): void {
    if (this.currentInterval) {
      clearInterval(this.currentInterval);
    }
  }
}
