# Project Functional Requirements

1. All text in the project should be in English.
2. This project must have:
   * a lot of things to click, send, receive
   * a lot of pages to go through
   * a lot of fields to fill in
   * a lot of API requests.
3. The project should consist of two parts: api, front.
   * The "api" is the backend part of the application.
   * "Front" is the interface part of the application.
4. The project must have a lot of frontend pages and backend communication:
   * minimum: 10 pages
   * working authorization
   * CRUD
5. Backend engineers should focus on the server side, and create an extensive database and background backend.
6. It is necessary to use MySQL or SQL Server, design a detailed database scheme using MySQL workbench, and use correct
   relations.
7. It is necessary to store API documentation in Postman.
8. It is possible to use any framework.
9. It is necessary to cover the system with the unit tests.
10. It is necessary to create a Readme file with the project overview and instructions.
11. UI, DB, API, documentation, and tickets must ideally match the code at the end of the project.
12. The system must provide basic authorization
13. Each user can register independently
14. The system has 2 roles: USER, ADMIN