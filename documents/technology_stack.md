# Project Technology Stack

## Backend
* Programming Language: JavaScript
* Framework: NestJs
* Database: PostgreSQL
* ORM: TypeORM
* API: Postman
* Authentication: PassportJs
* File storage: YandexS3

## Frontend
* Programming Language: JavaScript
* Framework: Angular
* UI library: Angular Material
* RxJs, NgRx
