# Project Technical Design

## Class BaseService<Entity extends ObjectLiteral>

#### function getBy({ key, value }: GetByDto, deleted?: boolean)

* `input` json
* `output` Promise<Model>
  * This function finds the model by the passed parameters

#### function getByMultipleProperties(getByDtos: GetByDto[])

* `input` json[]
* `output` Promise<Model>
  * This function finds the model by several passed parameters

#### function getByWithRelations({ key, value }: GetByDto, relations: string[])

* `input` json, json[]
* `output` Promise<Model>
  * This function finds the model by passed parameters with passed relations

#### function checkExist(id: number)

* `input` json
* `output` Promise<Model>
  * This function checks the data for existence

## Class AuthService

#### function signUp(res: Response, json: SignUpDto)

* `input` res, json
* `output` Promise<{user, token}>
    * This function creates a user and sets a refresh token in cookies

#### function login(res: Response, loginDto: AuthDto)

* `input` res, json
* `output` Promise<{user, token}>
  * This function validates credentials, returns User info and access token, sets refresh token to headers


#### function validateUser(email: string, password: string)

* `input` json
* `output` Promise<User>
  * This function is for user extraction and password verification, returns User info

#### function verifyPassword(password: string, hashedPassword: string)

* `input` json
* `output` void
  * This function checks if the password is correct and throws an error if the passwords do not match

#### function createTokens(id: number, role: string)

* `input` json
* `output` json
  * This function creates tokens, returns accessToken, refreshToken

#### function refreshToken(req: Request, res: Response)

* `input` req, res
* `output` json
  * This function generates a new token pair based on the refresh token, returns access token, sets refresh token to
    headers
 
#### function logout(req: Request, res: Response)

* `input` req, res
* `output` Promise<{message: string}>
  * This function allows the user to log out and removes the refresh token from cookies

#### function setToken(res: Response, tokens: TokensDto)

* `input` res, json
* `output` string
  * This function sets a refresh token in cookies and returns an access token

## Class UserService extends BaseService

#### function getOne(user: User)

* `input` user
* `output` Promise<User>
  * This function sets a refresh token in cookies and returns an access token 

#### function create(signUpDto: SignUpDto)

* `input` json
* `output` Promise<User>
  * This function creates a user
 
#### function createAdmins()

* `input` void
* `output` Promise<void>
  * This function creates an admin on the project init

#### function update(user: User, updateUserDto: UpdateUserDto)

* `input` Model, json
* `output` Promise<User>
  * This function updates user data
 
#### function updateEmail(user: User, updateEmailDto: UpdateEmailDto)

* `input` Model, json
* `output` Promise<User>
  * This function updates user email
 
#### function updatePassword(user: User, updatePasswordDto: UpdatePasswordDto)

* `input` Model, json
* `output` Promise<User>
  * This function updates user password

#### function encodePassword(password: string)

* `input` json
* `output` Promise<{password: string}>
  * This function encodes password

#### function removePasswordOnRes(user: User)

* `input` Model
* `output` User
  * This function deletes user password

## Class CardService extends BaseService

#### function create(user: User, createCardDto: CreateCardDto)

* `input` Model, json
* `output` Promise<Card>
  * This function creates card
 
#### function getByUserId(id: number)

* `input` json
* `output` Promise<Card>
  * This function finds card by user id

#### function create(user: User, createCardDto: CreateCardDto)

* `input` Model, json
* `output` Promise<Card>
  * This function creates card

#### function remove(userId: number)

* `input` json
* `output` Promise<void>
  * This function changes card status to DELETED

## Class CityService extends BaseService

#### function get()

* `input` void
* `output` Promise<City[]>
  * This function returns all cities with active status

#### function create(createCityDto: CreateCityDto)

* `input` json
* `output` Promise<City>
  * This function creates city

#### function update(id: number, updateCityDto: UpdateCityDto)

* `input` json
* `output` Promise<City>
  * This function updates city data

#### function remove(id: number)

* `input` json
* `output` Promise<{message: string}>
  * This function changes city status to DELETED

## Class DishService extends BaseService 

#### function getAll()

* `input` void
* `output` Promise<Dish[]>
  * This function returns all dishes with active status

#### function getByImageId(imageId: number)

* `input` json
* `output` Promise<Dish>
  * This function returns dish by image id

#### function create(createDishDto: CreateDishDto)

* `input` json
* `output` Promise<Dish>
  * This function creates dish

#### function update(id: number, updateDishDto: UpdateDishDto)

* `input` json
* `output` Promise<Dish>
  * This function updates dish data

## Class FileService extends BaseService 

#### function getFileById(id: number)

* `input` json
* `output` Promise<File>
  * This function returns file by id
 
#### function upload(file: Express.Multer.File, directory: string)

* `input` file, json
* `output` Promise<File>
  * This function uploads file 

#### function removeFileById(id: number)

* `input` json
* `output` Promise<{message: string}>
  * This function changes file status to DELETED

## Class LocationAddressService extends BaseService

#### function getAll()

* `input` void
* `output` Promise<LocationAddress[]>
  * This function returns all locations addresses with active status

#### function getByCityId(id: number)

* `input` json
* `output` Promise<LocationAddress>
  * This function returns location address by city id

#### function getByStateId(id: number) 

* `input` json
* `output` Promise<LocationAddress>
  * This function returns location address by state id

#### function updateManyByCity(id: number)

* `input` json
* `output` Promise<void>
  * This function updates locations addresses by city id

#### function updateManyByState(id: number)

* `input` json
* `output` Promise<void>
  * This function updates locations addresses by state id

#### function create(createLocationAddressDto: CreateLocationAddressDto)

* `input` json
* `output` Promise<LocationAddress>
  * This function creates location address

#### function update(id: number, updateLocationAddressDto: UpdateLocationAddressDto)

* `input` json
* `output` Promise<LocationAddress>
  * This function updates location address

#### function remove(id: number)

* `input` json
* `output` Promise<LocationAddress>
  * This function changes location address status to DELETED

## Class MenuService extends BaseService

#### function getAll()

* `input` void
* `output` Promise<Menu[]>
  * This function returns all menus with active status

#### function create(createMenuDto: CreateMenuDto)

* `input` json
* `output` Promise<Menu>
  * This function creates menu

#### function update(id: number, updateMenuDto: UpdateMenuDto)

* `input` json
* `output` Promise<Menu>
  * This function updates menu

## Class MenuSectionService extends BaseService

#### function getAll()

* `input` void
* `output` Promise<MenuSection[]>
  * This function returns all menu sections with active status

#### function create(createMenuSectionDto: CreateMenuSectionDto)

* `input` json
* `output` Promise<MenuSection>
  * This function creates menu section

#### function update(id: number, updateMenuSectionDto: UpdateMenuSectionDto)

* `input` json
* `output` Promise<MenuSection>
  * This function updates menu section

## Class StateService extends BaseService

#### function get()

* `input` void
* `output` Promise<State[]>
  * This function returns all states with active status

#### function create(createStateDto: CreateStateDto)

* `input` json
* `output` Promise<State>
  * This function creates state

#### function update(id: number, updateCityDto: UpdateCityDto)

* `input` json
* `output` Promise<State>
  * This function updates state

#### function remove(id: number)

* `input` json
* `output` Promise<{message: string}>
  * This function changes state status to DELETED 
